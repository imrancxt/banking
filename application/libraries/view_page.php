<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of view_page
 *
 * @author imran
 */
class view_page {

//put your code here
    public $ci;

    function __construct() {
        $this->ci = &get_instance();
    }

    function admin_page($page_name = "", $data) {
        $this->ci->load->view('vadmin/header.php',"");
        $this->ci->load->view("vadmin/$page_name", $data);
        $this->ci->load->view("vadmin/footer.php");
    }
    public function test_page($page_name = "", $data=""){
       
        $this->ci->load->view('theme/xtheme/header.php',"");
        $this->ci->load->view("vadmin/ajax_code/$page_name", $data);
        $this->ci->load->view("theme/xtheme/footer.php");
    }
    public function load_page($page_name = "", $data=""){
       
        //
       if(isset($_SESSION["banking_theme"])){
           $theme=$_SESSION["banking_theme"];
       }
       else{
           $theme="xtheme";
       }
       $theme="gentelella";
        $this->ci->load->view("theme/$theme/header.php","");
        $this->ci->load->view("vadmin/ajax_code/$page_name", $data);
        $this->ci->load->view("theme/$theme/footer.php");
    }
}

?>
