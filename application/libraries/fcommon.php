<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of fcommon
 *
 * @author imran.v2v
 */
class fcommon {

    //put your code here
    public function alert_message($message, $alert_type) {
        $icon = $this->return_icon($alert_type);
        echo"<div class='alert alert-$alert_type alert-dismissable' style='margin-bottom: 0px !important'>
                    <h4><i class='icon fa fa-$icon'></i> Alert!</h4>
                    $message
                  </div>";
        
    }

    private function return_icon($alert_type) {
        switch ($alert_type) {
            case 'success':
                return 'check';
            case 'danger':
                return 'close';
            default:
                break;
        }
    }

    public function get_account_type_table($account_name) {
        switch ($account_name) {
            case 'Loan':
                return array('account_table'=>'loan_account','ledger_account'=>'loan_account_ledger','acc_type'=>'asset');//'';'loan_account';
            case 'S.B':
                return array('account_table'=>'savings_account','ledger_account'=>'savings_account_ledger','acc_type'=>'liability');;
            default:
                 return array('account_table'=>'others_account','ledger_account'=>'others_account_ledger','acc_type'=>'asset');
        }
    }
    public function get_account_name_for_add_supplimentary($account_name,$trans_type){
        if(($account_name=="Loan" || $account_name=="S.B") && ($trans_type=="interest" || $trans_type=="service charge")){
            return "Income A/C";
        }
        else{
            return $account_name;
        }

    }
    public function get_account_type($trans_type){
        if($trans_type=="interest" || $trans_type=="service charge"){
            return "asset";
        }
    }

}

?>
