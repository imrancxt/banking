<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of member
 *
 * @author imran.v2v
 */
class member extends CI_Controller {

    //put your code here
    function __construct() {
         session_start();
        if(!isset($_SESSION['admin'])){
            parent::__construct();
        }
        parent::__construct();
        //echo"test";
        $this->load->library('view_page');
        $this->load->model('madmin');
        $this->load->library('fcommon');
    }

    function index() {
        $this->view_page->admin_page('testing', "");
    }

    public function add_member() {
        $this->view_page->load_page('v_add_member');
    }

    public function current_member() {
        $query = "SELECT * FROM `member_info` where status='current'";

        $data = $this->madmin->get_data($query, array('member_no', 'name', 'father_name', 'mother_name', 'current_address', 'phone_no', 'opening_date', 'occupation', 'status'), array('member_no', 'name', 'father_name', 'mother_name', 'current_address', 'phone_no', 'opening_date', 'occupation', 'status'));
        //$this->load->view('vadmin/ajax_code/v_current_member', $data);
         $this->view_page->load_page('v_current_member',$data);
    }

    public function open_savings_account() {
        $query = "SELECT member_no FROM `member_info` where status='current'";
        $data = $this->madmin->get_data($query, array('member_no'), array('member_no'));
        //$this->load->view('vadmin/ajax_code/v_open_savings_account', $data);
        $this->view_page->load_page('v_open_savings_account',$data);
    }

    public function check_member($member_no) {
        $query = "SELECT * FROM `member_info` where member_no='$member_no'";
        $data = $this->get_member_info($query);
        $data['account_type'] = "savings_account";
        $this->load->view('vadmin/ajax_code/v_get_member_info', $data);
    }

    public function check_savings_account_member($account_no) {
        $query = "SELECT t2.* FROM `savings_account` as t1,member_info as t2 where t1.member_no=t2.member_no and t1.account_number='$account_no'";
        $data = $this->get_member_info($query);
        $data['account_type'] = "loan_account";
        $this->load->view('vadmin/ajax_code/v_get_member_info', $data);
    }

    private function get_member_info($query) {
        $data = $this->madmin->get_data($query, array('member_no', 'name', 'father_name', 'mother_name', 'current_address', 'permanent_address', 'phone_no', 'age', 'nationality', 'occupation','profile_img','signature_img'), array('member_no', 'name', 'father_name', 'mother_name', 'current_address', 'permanent_address', 'phone_no', 'age', 'nationality', 'occupation','profile_img','signature_img'));
        return $data;
    }

    public function get_account_info($supplementary_name) {
        $table = $this->fcommon->get_account_type_table($supplementary_name);
        $query = "SELECT * FROM `{$table['account_table']}` where status='current'";
        $data = $this->madmin->get_data($query, array('member_no', 'account_no','opening_amount'), array('member_no', 'account_number','opening_amount'));
        $data['account_name'] = $supplementary_name;
        $data['ledger_account'] = $table['ledger_account'];
        $data['acc_type'] = $table['acc_type'];
        $this->load->view('vadmin/ajax_code/v_get_account_info', $data);

        //print_r($data);
    }

    public function open_loan_account() {
        $query = "SELECT account_number FROM `savings_account` where status='current'";
        $data = $this->madmin->get_data($query, array('saving_account_number'), array('account_number'));
       // $this->load->view('vadmin/ajax_code/v_open_loan_account', $data);
        $this->view_page->load_page('v_open_loan_account',$data);
        
    }

    public function all_savings_account() {
        $query = "SELECT t3.status,t2.name, t1.member_no,t1.account_no,sum(t1.dr) as dr,sum(t1.cr) as cr,t3.opening_amount,t3.opening_date FROM 
               `savings_account_ledger` as t1,savings_account as t3,
                member_info as t2 where t1.member_no=t2.member_no and t1.account_no=t3.account_number and t1.trans_type='nett' group by t1.account_no";
        $data = $this->madmin->get_data($query, array('status', 'name', 'member_no', 'account_no', 'dr', 'cr', 'opening_amount', 'opening_date'), array('status', 'name', 'member_no', 'account_no', 'dr', 'cr', 'opening_amount', 'opening_date'));
        //$this->load->view('vadmin/ajax_code/v_all_savings_account', $data);
        $this->view_page->load_page('v_all_savings_account',$data);
    }

    public function all_loan_account() {
        $query = "SELECT t3.status, t2.name, t1.member_no,t1.account_no,sum(t1.dr) as dr,sum(t1.cr) as cr,t3.opening_date FROM 
               `loan_account_ledger` as t1,loan_account as t3,
                member_info as t2 where t1.member_no=t2.member_no and t1.account_no=t3.account_number and t1.trans_type='nett' group by t1.account_no";
        $data = $this->madmin->get_data($query, array('status', 'name', 'member_no', 'account_no', 'dr', 'cr', 'opening_date'), array('status', 'name', 'member_no', 'account_no', 'dr', 'cr', 'opening_date'));
        //$this->load->view('vadmin/ajax_code/v_all_loan_account', $data);
        $this->view_page->load_page('v_all_loan_account',$data);
    }

    public function deactive_member($member_no) {
        $this->change_member_status($member_no, "previous", "current", "Previous");
    }

    private function change_member_status($member_no, $set, $con, $messege) {
        $this->db->trans_start();
        $this->db->update('member_info', array('status' => $set), array('member_no' => $member_no, 'status' => $con));
        $this->db->update('savings_account', array('status' => $set), array('member_no' => $member_no, 'status' => $con));
        $this->db->update('loan_account', array('status' => $set), array('member_no' => $member_no, 'status' => $con));
        echo $messege;
        $this->db->trans_complete();
    }

    public function previous_member() {
        $query = "SELECT * FROM `member_info` where status='previous'";
        $data = $this->madmin->get_data($query, array('member_no', 'name', 'father_name', 'mother_name', 'current_address', 'phone_no', 'opening_date', 'occupation', 'status'), array('member_no', 'name', 'father_name', 'mother_name', 'current_address', 'phone_no', 'opening_date', 'occupation', 'status'));
        $this->view_page->load_page('v_previous_member', $data);
    }

    public function active_member($member_no) {
        $this->change_member_status($member_no, "current", "previous", "Current");
    }
    public function profile(){
        $id=$_GET['id'];
        $query1="SELECT * FROM `member_info` where member_no='$id'";
        $data1=$this->get_member_info($query1);

        $query2="SELECT t1.account_number,t1.opening_date FROM `loan_account` as t1 where t1.member_no='$id'";
        $data2=$this->madmin->get_data($query2,array('loan_acc_number','loan_opening_date'),array('account_number','opening_date'));

        $query3="SELECT t1.account_number,t1.opening_date FROM `savings_account` as t1 where t1.member_no='$id'";
        $data3=$this->madmin->get_data($query3,array('savings_acc_number','savings_opening_date'),array('account_number','opening_date'));

        $data=array_merge($data1,$data2,$data3);
        $this->view_page->load_page('v_profile',$data);
    }

}

?>
