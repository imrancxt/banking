<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of panel
 *
 * @author imran.v2v
 */
class panel extends CI_Controller {
    //put your code here
    
    //put your code here
    function __construct() {
         session_start();
        if(!isset($_SESSION['admin'])){
            $url="../login/admin_login/";
            header("Location: $url");
        }
        parent::__construct();
        //echo"test";
        $this->load->library('view_page');
        $this->load->model('madmin');
    }

    function index() {
        $info['morris_data1'] = $this->get_account_info();
        $info['morris_data2'] = $this->get_account_drcr();
        $info['morris_data3'] = $this->individual_drcr("S.B");
        $info['morris_data4'] = $this->individual_drcr("Loan");
        // prin
        //print_r($info);SELECT date,sum(dr) as dr,sum(cr) as cr FROM `general_ledger` where account_name='S.B' group by date
        $this->view_page->load_page('v_dash', $info);
    }

    private function individual_drcr($account_name) {
        $query = "SELECT date,sum(dr) as dr,sum(cr) as cr FROM `general_ledger` where account_name='$account_name' group by date";
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $data[$i] = array('y' => $row->date, 'a' => $row->dr, 'b' => $row->cr);
                $i++;
            }
        }
        return $data;
    }

    private function get_account_info() {
        $query = "SELECT count(member_no) as loan_account,(select count(member_no) from savings_account) as savings_account FROM `loan_account`";
        $label = array('Loan Account', 'Savings Account');
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                $data[0] = array('label' => $label[0], 'value' => $row->loan_account);
                $data[1] = array('label' => $label[1], 'value' => $row->savings_account);
            }
        }
        return $data;
    }

    private function get_account_drcr() {
        $query = "SELECT account_name,sum(dr) as dr,sum(cr) as cr FROM `general_ledger` group by account_name";
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $data[$i] = array('y' => $row->account_name, 'a' => $row->dr, 'b' => $row->cr);
                $i++;
            }
        }
        return $data;
    }

    public function profile() {
        if($_SESSION['user_type']=='admin'){
             $this->view_page->load_page('v_admin_profile', $info);
        }
       
    }
    public function update_bank_name(){
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $query1="SELECT serial FROM `admin` where name='{$_SESSION['admin']}' and password='{$data['password']}'";
            if($this->madmin->get_single_data($query1,'serial')!=0){
                $this->db->update('admin',array('bank_name'=>$data['bank_name'],'address'=>$data['address'],'phone_no'=>$data['phone_no']));
                $_SESSION['bank_name']=$data['bank_name'];
                $_SESSION['address']=$data['address'];
                $_SESSION['phone_no']=$data['phone_no'];
                echo"refresh";
            }
           // echo $query1;
        }
        public function user_management(){
            $query="SELECT * FROM `admin`";
            $data=$this->madmin->get_data($query,array('serial','user_type','name','password'),array('serial','user_type','name','password'));
            $this->view_page->load_page('v_user_management', $data);
            
        }
        public function update_user_info(){
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
           // $this->db->update('admin',$data,array('serial'=>$data['serial']));
            $this->db->update('admin',$data);
            echo "refresh";
        }
        public function add_user_info(){
            $this->input->post(NULL, TRUE);
            $data = $this->input->post();
            $this->db->insert('admin',$data);
            echo "refresh";
        }
        public function delete_user(){
            $serial=$_GET['serial'];
            $this->db->delete('admin',array('serial'=>$serial));
            echo"refresh";
        }
         public function theme($theme){
            $_SESSION['banking_theme']=$theme;
            echo"refresh";
        }
}

?>
