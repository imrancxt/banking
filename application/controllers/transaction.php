<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of transaction
 *
 * @author imran.v2v
 */
class transaction extends CI_Controller {

    //put your code here
    function __construct() {
        session_start();
        if (!isset($_SESSION['admin'])) {
            exit();
        }
        parent::__construct();
        $this->load->library('view_page');
        $this->load->model('madmin');
        $this->load->library('fcommon');
    }

    public function open_supplementary() {
        //$this->load->view('vadmin/ajax_code/v_open_supplementary',"");
        $this->view_page->load_page('v_open_supplementary', "");
    }

    public function get_general_ledger_data() {
        $account_type = $_GET['account'];
        $month = date("Y-m");
        /*$query = "SELECT * FROM `general_ledger` where account_name='$account_type'and left(`date`,7)='$month'  order by date";
        $data = $this->madmin->get_data($query, array('date', 'dr', 'cr', 'particular'), array('date', 'dr', 'cr', 'particular'));
        $date="$month-01";
        $query2="SELECT (sum(cr)-sum(dr)) as opening_balance FROM `general_ledger` where account_name='$account_type'and date<'$date'";
        $data["opening_balance"]=$this->general_ledger_last_month_balance($query2);
        $data['account_type'] = "($month):" . $account_type. " Account<br>Last Month Balance: ".$data["opening_balance"];
        */
        $data=$this-> get_general_ledger_for_special_account($account_type,$month);
        $data['account_type2'] = $account_type;
        $this->view_page->load_page('v_get_general_ledger_data2', $data);
    }
    private function get_general_ledger_for_special_account($account_type,$month){
        $query = "SELECT * FROM `general_ledger` where account_name='$account_type'and left(`date`,7)='$month'  order by date";
        $data = $this->madmin->get_data($query, array('date', 'dr', 'cr', 'particular'), array('date', 'dr', 'cr', 'particular'));
        $date="$month-01";
        $query2="SELECT (sum(cr)-sum(dr)) as opening_balance FROM `general_ledger` where account_name='$account_type'and date<'$date'";
        $data["opening_balance"]=$this->general_ledger_last_month_balance($query2);
        $data['account_type'] = "($month):" . $account_type. " Account<br>Last Month Balance: ".$data["opening_balance"];
        return $data;
    }

    public function filter_get_general_ledger_data() {
        $account_type = $_GET['account'];
        $month = $_GET['month'];
        $data=$this->get_general_ledger_for_special_account($account_type,$month);
        $this->load->view('vadmin/v_filter_get_general_ledger_data', $data);
    }
    
    private function general_ledger_last_month_balance($query){
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                if ($row->opening_balance != NULL) {
                    return $row->opening_balance;
                } else {
                    return 0;
                }
            }
        }
    }

    public function get_indivisual_account() {
        $account_type = $_GET['account_type'];
        $account_no = $_GET['account_no'];
        $table = $this->fcommon->get_account_type_table($account_type);

        $data1 = $this->get_nett_ledger($table['ledger_account'], $account_no);
        $data2 = $this->get_interest_ledger($table['ledger_account'], $account_no);
        $data3 = $this->get_service_ledger($table['ledger_account'], $account_no);

        //$query1 = "SELECT * FROM `{$table['ledger_account']}`where account_no='$account_no' and trans_type='nett' order by date";
        //$data1 = $this->madmin->get_data($query1, array('dr', 'cr', 'date', 'trns_comment','particular'), array('dr', 'cr', 'date', 'comment','particular'));
        // $query2 = "SELECT t1.account_number,t1.opening_amount,t1.opening_date as sb_opening_date,t1.comment, t2.* FROM `{$table['account_table']}` as t1, `member_info` as t2 where t1.member_no=t2.member_no and t1.account_number='$account_no'";
        $data4 = $this->get_account_basic_query($account_type, $table['account_table'], $account_no); //$this->madmin->get_data($query2, array('member_no', 'account_number', 'opening_amount', 'opening_date', 'sb_opening_date', 'comment', 'name', 'father_name', 'mother_name', 'current_address', 'permanent_address', 'birth_date', 'age', 'nationality', 'religion', 'occupation', 'education', 'phone_no', 'status'), array('member_no', 'account_number', 'opening_amount', 'opening_date', 'sb_opening_date', 'comment', 'name', 'father_name', 'mother_name', 'current_address', 'permanent_address', 'birth_date', 'age', 'nationality', 'religion', 'occupation', 'education', 'phone_no', 'status'));
        $data5 = $this->get_book_ledger($table['ledger_account'], $account_no);
        $data = array_merge($data1, $data2, $data3, $data4,$data5);
        $data['account_type'] = $account_type;
        $data['trans_date'] = "All";
        //print_r($data);
        $this->view_page->load_page('v_get_indivisual_account', $data);
    }

    private function get_book_ledger($table, $account_no, $date1 = "", $date2 = "") {
        $query = "SELECT * FROM `$table`where account_no='$account_no' and trans_type='book' order by date";
        if ($date1 != "" && $date2 != "") {

            $query = "SELECT * FROM `$table`where account_no='$account_no' and trans_type='book' and date between '$date1' and '$date2' order by date";
            //echo $query;
        }
        $data = $this->madmin->get_data($query, array('book_dr', 'book_cr', 'book_date', 'book_trns_comment', 'book_particular'), array('dr', 'cr', 'date', 'comment', 'particular'));
        
        $query2="SELECT (sum(cr)-sum(dr)) as opening_amount FROM `$table`where account_no='$account_no' and trans_type='book' and date<'$date1'";
        $data['book_opeing_balance']=$this->general_ledger_last_month_balance($query2);
        return $data;
    }

    private function get_nett_ledger($table, $account_no, $date1 = "", $date2 = "") {

        $query = "SELECT * FROM `$table`where account_no='$account_no' and trans_type='nett' order by date";
        if ($date1 != "" && $date2 != "") {

            $query = "SELECT * FROM `$table`where account_no='$account_no' and trans_type='nett' and date between '$date1' and '$date2' order by date";
            //echo $query;
        }
        $data = $this->madmin->get_data($query, array('nett_dr', 'nett_cr', 'nett_date', 'nett_trns_comment', 'nett_particular'), array('dr', 'cr', 'date', 'comment', 'particular'));
        $query2="SELECT (sum(cr)-sum(dr)) as opening_amount FROM `$table`where account_no='$account_no' and trans_type='nett' and date<'$date1'";
        $data['nett_opeing_balance']=$this->general_ledger_last_month_balance($query2);
        return $data;
    }

    private function get_interest_ledger($table, $account_no, $date1 = "", $date2 = "") {
        $query = "SELECT * FROM `$table`where account_no='$account_no' and trans_type='interest' order by date";
        if ($date1 != "" && $date2 != "") {
            $query = "SELECT * FROM `$table`where account_no='$account_no' and trans_type='interest' and date between '$date1' and '$date2' order by date";
        }
        $data = $this->madmin->get_data($query, array('interest_dr', 'interest_cr', 'interest_date', 'interest_trns_comment', 'interest_particular'), array('dr', 'cr', 'date', 'comment', 'particular'));
        
        $query2="SELECT (sum(cr)-sum(dr)) as opening_amount FROM `$table`where account_no='$account_no' and trans_type='interest' and date<'$date1'";
        $data['interest_opeing_balance']=$this->general_ledger_last_month_balance($query2);
        
        return $data;
    }

    private function get_service_ledger($table, $account_no, $date1 = "", $date2 = "") {
        $query = "SELECT * FROM `$table`where account_no='$account_no' and trans_type='service charge' order by date";
        if ($date1 != "" && $date2 != "") {
            $query = "SELECT * FROM `$table`where account_no='$account_no' and trans_type='service charge' and date between '$date1' and '$date2' order by date";
        }
        $data = $this->madmin->get_data($query, array('service_dr', 'service_cr', 'service_date', 'service_trns_comment', 'service_particular'), array('dr', 'cr', 'date', 'comment', 'particular'));
        
        $query2="SELECT (sum(cr)-sum(dr)) as opening_amount FROM `$table`where account_no='$account_no' and trans_type='service charge' and date<'$date1'";
        $data['service_opeing_balance']=$this->general_ledger_last_month_balance($query2);
        return $data;
    }

    private function get_account_basic_query($account_type, $table, $account_no) {
        $query = "";
        switch ($account_type) {
            case 'S.B':
                $query = "SELECT t1.account_number,t1.opening_amount,t1.opening_date as sb_opening_date,t1.comment, 
                t2.* FROM `$table` as t1, `member_info` as t2 where t1.member_no=t2.member_no and t1.account_number='$account_no'";
                return $this->madmin->get_data($query, array('member_no', 'account_number', 'opening_amount', 'opening_date', 'sb_opening_date', 'comment', 'name', 'father_name', 'mother_name', 'current_address', 'permanent_address', 'birth_date', 'age', 'nationality', 'religion', 'occupation', 'education', 'phone_no', 'status'), array('member_no', 'account_number', 'opening_amount', 'opening_date', 'sb_opening_date', 'comment', 'name', 'father_name', 'mother_name', 'current_address', 'permanent_address', 'birth_date', 'age', 'nationality', 'religion', 'occupation', 'education', 'phone_no', 'status'));
            case 'Loan':
                $query = "SELECT t1.account_number,t1.opening_date as sb_opening_date,t1.comment, t1.opening_amount as interest_amount,
                t2.* FROM `$table` as t1, `member_info` as t2 where t1.member_no=t2.member_no and t1.account_number='$account_no'";
                return $this->madmin->get_data($query, array('member_no', 'account_number', 'opening_date', 'sb_opening_date', 'comment', 'name', 'father_name', 'mother_name', 'current_address', 'permanent_address', 'birth_date', 'age', 'nationality', 'religion', 'occupation', 'education', 'phone_no', 'status', 'interest_amount'), array('member_no', 'account_number', 'opening_date', 'sb_opening_date', 'comment', 'name', 'father_name', 'mother_name', 'current_address', 'permanent_address', 'birth_date', 'age', 'nationality', 'religion', 'occupation', 'education', 'phone_no', 'status', 'interest_amount'));
            default:
                return 0;
        }
    }

    public function get_ledger_summary() {
        $trans_type = $_GET['trans_type'];
        $date = date("Y-m-d");
        $data = $this->get_ledger_summary_data($trans_type, $date);
        $data['trans_type'] = $trans_type;
        $this->view_page->load_page('v_get_ledger_summary', $data);
    }

    private function get_ledger_summary_data($trans_type, $date) {
        $query = "SELECT account_name,sum($trans_type) as cash FROM `general_ledger` where date='$date' group by account_name";
        return $this->madmin->get_data($query, array('account_name', 'cash'), array('account_name', 'cash'));
    }

    public function filter_summary($trans_type, $date) {
        $data = $this->get_ledger_summary_data($trans_type, $date);
        $this->load->view('vadmin/ajax_code/v_filter_summary', $data);
    }

    public function open_supplementary2() {
        $query1 = "SELECT distinct(acc_head) FROM `acc_subhead`";
        $data1 = $this->madmin->get_data($query1, array('acc_head'), array('acc_head'));
        $data2 = array();
        if (isset($data1)) {
            $query2 = "SELECT * FROM `acc_subhead` where acc_head='{$data1['acc_head'][0]}'";
            $data2 = $this->madmin->get_data($query2, array('acc_subhead'), array('acc_subhead'));
        }
        $data = array_merge($data1, $data2);
        //$this->load->view('vadmin/ajax_code/v_open_supplementary2', $data);
        $this->view_page->load_page('v_open_supplementary2', $data);
    }

    public function add_supplementary_data2() {
        
    }

    public function others_account() {
        $query = "SELECT acc_head FROM `account_head`";
        $data = $this->madmin->get_data($query, array('acc_head'), array('acc_head'));
        $this->view_page->load_page('v_others_account', $data);
        //$this->load->view('vadmin/ajax_code/v_others_account', $data);
    }

    public function get_others_account_ledger_data() {
        $account_type = $_GET['acc_head'];
        $month = $_GET['month'];
        $data=$this->get_general_ledger_for_special_account($account_type,$month);
        $this->load->view('vadmin/ajax_code/v_get_general_ledger_data', $data);
    }

    public function filter_customer_transaction($acc_type, $account_no, $date1, $date2) {
        $table = $this->fcommon->get_account_type_table($acc_type);
        $data1 = $this->get_nett_ledger($table['ledger_account'], $account_no, $date1, $date2);
        $data2 = $this->get_interest_ledger($table['ledger_account'], $account_no, $date1, $date2);
        $data3 = $this->get_service_ledger($table['ledger_account'], $account_no, $date1, $date2);
        $data4 = $this->get_book_ledger($table['ledger_account'], $account_no, $date1, $date2);
        $data = array_merge($data1, $data2, $data3,$data4);
        $data["trans_date"] = "$date1 To $date2";
        $this->load->view('vadmin/common_page/v_filter_customer_transaction', $data);
    }

    public function others_account_subhead() {
        $query = "SELECT acc_head FROM `account_head`";
        $data = $this->madmin->get_data($query, array('acc_head'), array('acc_head'));
        $this->view_page->load_page('v_others_account_subhead', $data);
    }

    public function get_acc_sub_head() {
        $acc_head = $_GET['acc_head'];
        $query = "SELECT distinct(acc_subhead) FROM `general_ledger` where account_name='$acc_head'";
        $data = $this->madmin->get_data($query, array('acc_subhead'), array('acc_subhead'));
        // echo"<option>k</option>";
        if (isset($data)) {
            foreach ($data['acc_subhead'] as $option) {
                echo"<option>$option</option>";
            }
        }
    }

    public function get_acc_subhead_data() {
        $acc_head = $_GET['acc_head'];
        $acc_subhead = $_GET['acc_subhead'];
        $month = $_GET['month'];
        $query = "SELECT * FROM `general_ledger` where account_name='$acc_head' and acc_subhead='$acc_subhead' and left(`date`,7)='$month'";
        $data = $this->madmin->get_data($query, array('date', 'dr', 'cr', 'particular'), array('date', 'dr', 'cr', 'particular'));
        $date="$month-01";
        $query2="SELECT (sum(cr)-sum(dr)) as opening_balance FROM `general_ledger` where account_name='$acc_head' and acc_subhead='$acc_subhead' and date<'$date'";
        $data["opening_balance"]=$this->general_ledger_last_month_balance($query2);
        $data['account_type'] = "($month):" . " $acc_subhead($acc_head)". " Account<br>Last Month Balance: ".$data["opening_balance"];
        $this->load->view('vadmin/ajax_code/v_get_general_ledger_data', $data);
        //echo $query;
    }

   

}

?>
