<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of upload
 *
 * @author imran.v2v
 */
class upload extends CI_Controller {

    //put your code here
    function __construct() {
        session_start();
        if (!isset($_SESSION['admin'])) {
            exit();
        }
        parent::__construct();
        $this->load->library('fcommon'); //load common function library
        $this->load->model('madmin');
        $this->userid = 1;
    }

    public function add_member() {
        $this->input->post(NULL, TRUE);
        $data = $this->input->post();

        /* if ($this->db->insert('member_info', $data)) {
          $this->fcommon->alert_message("A New Member Added!", "success");
          } else {
          $this->fcommon->alert_message("Please Insert Correct Information", "danger");
          } */

        if ($this->check_account("select member_no from `member_info` where member_no='{$data['member_no']}'") == 0) {
            $this->fcommon->alert_message("This Member Is Already Exists!", "danger");
        } else {
            if ($this->db->insert('member_info', $data)) {
                $this->fcommon->alert_message("A New Member Added!", "success");
            }
        }
    }

    public function add_supplementary_data() {
        //GET BASCI INFO
        $this->input->post(NULL, TRUE);
        $data = $this->input->post();
        $account_info = array();
        $general_ledger_info = array();
        $total_amount = 0;
        $time = date('Y-m-d h:i:s');
        $all_special_trans_type=array("service charge","interest","book");
        $date = $data['date'];
        $dr_cr = $data['supplementary_type'];//selected supplimentary type(dr or cr)
        
        //$supplementary_name = $this->fcommon->get_account_name_for_add_supplimentary($data['supplementary_name'], $data['trans_type']); 
        
        $ledger_table = $data['ledger_account'];//special account ledger table like loan_account_ledger,savings_account_ledger etc.
        $acc_type = $data['acc_type'];
        
        /*if selected transaction type is in $all_special_trans_type then account_name of general_ledger will be 'Income A/C' and acc_type will be
        'liability'*/
        if (in_array($data["trans_type"],$all_special_trans_type)) {
            $acc_type = "liability";
            $supplementary_name="Income A/C";
        }
        else{
            $supplementary_name=$data['supplementary_name'];
        }
        //end
        
        //if selected supplementary type is dr then particular will be 'To'  otherwise 'By'
        if ($dr_cr == "dr") {
            $particular = "To";
        } else {
            $particular = "By";
        }
        //end
        
        //SET ALL SELECTED ACCOUNT FOR special account ledger table like loan_account_ledger,savings_account_ledger etc.
        for ($i = 0; $i < count($data['account_no']); $i++) {
            $account_no = $data['account_no'][$i];
            $cash = $data["cash_$account_no"];
            $transfer = $data["transfer_$account_no"];
            $total = $cash + $transfer;
            $comment = $data["comment_$account_no"];
            $member_no = $data["member_$account_no"];

            $account_info[$i] = array('date' => $date, 'member_no' => $member_no, 'account_no' => $account_no, $dr_cr => $total, 'comment' => $comment,
                'cash' => $cash, 'transfer' => $transfer, 'time' => $time, 'userid' => $this->userid, 'particular' => $particular, 'trans_type' => $data['trans_type']);
            $total_amount+=$total;
        }
        //end
        
        //SET GENERAL LEDGER INFO
        $general_ledger_info = array('account_name' => $supplementary_name, 'date' => $date, $dr_cr => $total_amount, 'acc_type' => $acc_type
            , 'time' => $time, 'userid' => $this->userid, 'particular' => $particular, 'acc_subhead' => $data['trans_type']);
        //end
       
        //update interest ledger info
        $update_interest = array();
        if ($data['supplementary_name'] == "Loan" && $data['trans_type'] == "interest") {
            for ($i = 0; $i < count($data['account_no']); $i++) {
                $account_no = $data['account_no'][$i];
                $cash = $data["cash_$account_no"];
                $transfer = $data["transfer_$account_no"];
                $total = $this->check_loan_interest($account_no) - $cash - $transfer;
                $amount2 = $cash + $transfer;
                $comment = $data["comment_$account_no"];
                $update_interest[$i] = array('account_number' => $account_no, 'opening_amount' => $total);
            }
        }
        //end
        
        /*if selected transaction type is in $all_special_trans_type then special account will be 'others_account_ledger'*/
        $others_account_ledger = array();
        if (in_array($data["trans_type"],$all_special_trans_type)) {
            for ($i = 0; $i < count($data['account_no']); $i++) {
                $account_no = $data['account_no'][$i];
                $cash = $data["cash_$account_no"];
                $transfer = $data["transfer_$account_no"];
                $amount2 = $cash + $transfer;
                $comment = $data["comment_$account_no"];
                $others_account_ledger[$i] = array($dr_cr => $amount2, 'acc_type' => 'Income A/C', 'date' => $date, 'time' => $time, 'acc_subhead' => $data['trans_type'], 'particular' => $particular,
                    'cash' => $cash, 'transfer' => $transfer, 'comment' => $comment);
            }
        }
        //end
        
        //Runing database query
        $this->db->trans_start();
        $this->db->insert('general_ledger', $general_ledger_info);
        $this->db->insert_batch($ledger_table, $account_info);
        if ($data['supplementary_name'] == "Loan" && $data['trans_type'] == "interest") {
            $this->db->update_batch('loan_account', $update_interest, 'account_number');
        }
        if($data['trans_type'] == "service charge" ||$data['trans_type']=="book"){
            $this->db->insert_batch('others_account_ledger',$others_account_ledger);
        }
        $this->fcommon->alert_message("Transaction Completed!", "success");
        $this->db->trans_complete();
        //end
    }

    private function check_loan_interest($account_no) {
        $query = "SELECT opening_amount FROM `loan_account` where account_number='$account_no'";
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                return $row->opening_amount;
            }
        } else {
            return 0;
        }
    }

    public function open_saving_account() {
        $this->input->post(NULL, TRUE);
        $data = $this->input->post();
        if ($this->check_account("select member_no from `savings_account` where account_number='{$data['account_number']}'") == 0) {
            $this->fcommon->alert_message("This Account Is Already Exists!", "danger");
        } else {
            if ($this->db->insert('savings_account', $data)) {
                $this->fcommon->alert_message("New Account Created Successfully", "success");
            }
        }
    }

    public function open_loan_account() {
        $this->input->post(NULL, TRUE);
        $data = $this->input->post();
        $query = "select member_no from `loan_account` where account_number='{$data['account_number']}'";
        if ($this->check_account("select member_no from `loan_account` where account_number='{$data['account_number']}'") == 0) {
            $this->fcommon->alert_message("This Account Is Already Exists!", "danger");
        } else {
            if ($this->db->insert('loan_account', $data)) {
                $this->fcommon->alert_message("New Account Created Successfully", "success");
            }
        }
        //echo $query;
    }

    private function check_account($query) {
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public function add_acc_type() {
        $this->input->post(NULL, TRUE);
        $data = $this->input->post();
        //echo"n";
        if ($this->db->insert('account_head', $data)) {
            $this->fcommon->alert_message("Account Head Added Successfully!", "success");
            //echo"Account Head Added Successfully!";
        }
        else{
            $this->fcommon->alert_message("Failed!", "danger");
        }
        
    }

    public function add_supplementary_data2() {
        $this->input->post(NULL, TRUE);
        $data = $this->input->post();
        $acc_type = $this->check_acc_head_type($data['supplementary_name']);
        $time = date('Y-m-d h:i:s');
        $amount = $data['cash'] + $data['transfer'];
        if ($data['drcr'] == "dr") {
            $particular = "To";
        } else {
            $particular = "By";
        }
        //general ledger
        $info = array('account_name' => $data['supplementary_name'], 'date' => $data['date'], $data['drcr'] => $amount, 'userid' => $this->userid,
            'acc_type' => $acc_type, 'time' => $time, 'acc_subhead' => $data['acc_subhead'], 'particular' => $particular);


        //Other account ledger
        $info2 = array($data['drcr'] => $amount, 'acc_type' => $data['supplementary_name'], 'date' => $data['date'], 'time' => $time, 'acc_subhead' => $data['acc_subhead'],
            'particular' => $particular, 'cash' => $data['cash'], 'transfer' => $data['transfer'], 'comment' => $data['comment']);

        $this->db->trans_start();
        $this->db->insert('others_account_ledger', $info2);
        $this->db->insert('general_ledger', $info);
        $this->fcommon->alert_message("Supplementary Data Inserted Successfully!", "success");
        $this->db->trans_complete();
    }

    private function check_acc_head_type($acc_head) {
        $query = "SELECT acc_type FROM `account_head` where acc_head='$acc_head' limit 1";
        $data = $this->madmin->get_data($query, array('acc_type'), array('acc_type'));
        return $data['acc_type'][0];
    }

    public function add_acc_subhead() {
        $this->input->post(NULL, TRUE);
        $data = $this->input->post();
        if ($this->db->insert('acc_subhead', $data)) {
             //echo"Account Head Added Successfully!";
            $this->fcommon->alert_message("New Account Sub-Head Created Successfully", "success");
        } else {
            $this->fcommon->alert_message("Error Encountered!", "danger");
        }
    }

    public function upload_loan_profit() {
        $this->input->post(NULL, TRUE);
        $data = $this->input->post();
        $query1 = "update loan_account set `opening_amount`=opening_amount+{$data['amount']} where account_number='{$data['loan_account_no']}'";
        $this->db->trans_start();
        $this->db->insert('set_loan_return', $data);
        $this->db->query($query1);
        $this->fcommon->alert_message("Transaction Completed", "success");
        $this->db->trans_complete();
        //echo $query1;
    }

    public function update_profile_pic() {
        if (isset($_POST['member_id'])) {
            $path = "img/profile/";
            if ((($_FILES["image"]["type"] == "image/gif")
                    || ($_FILES["image"]["type"] == "image/jpeg")
                    || ($_FILES["image"]["type"] == "image/pjpeg")
                    || ($_FILES["image"]["type"] == "image/png"))) {
                $new_image_name = "{$_POST['std_id']}" . ".jpg";
                if ($_FILES["image"]["error"] > 0) {
                    echo "Return Code: " . $_FILES["image"]["error"] . "<br />";
                } else {
                    $info = array('profile_img' => $new_image_name);
                    $this->db->update('member_info', $info, array('member_no' => $_POST['member_id']));
                    $temp_image = move_uploaded_file($_FILES["image"]["tmp_name"], $path . $_FILES["image"]["name"]);
                    //$url = "../public/img/profile/$new_image_name";
                    if (rename($path . $_FILES['image']['name'], $path . $new_image_name) && $temp_image == true) {
                        echo"refresh";
                    }
                }
            }
        }
    }

    public function update_signature_pic() {
        if (isset($_POST['member_id'])) {
            $path = "img/signature/";
            if ((($_FILES["image"]["type"] == "image/gif")
                    || ($_FILES["image"]["type"] == "image/jpeg")
                    || ($_FILES["image"]["type"] == "image/pjpeg")
                    || ($_FILES["image"]["type"] == "image/png"))) {
                $new_image_name = "{$_POST['std_id']}" . ".jpg";
                if ($_FILES["image"]["error"] > 0) {
                    echo "Return Code: " . $_FILES["image"]["error"] . "<br />";
                } else {
                    $info = array('signature_img' => $new_image_name);
                    $this->db->update('member_info', $info, array('member_no' => $_POST['member_id']));
                    $temp_image = move_uploaded_file($_FILES["image"]["tmp_name"], $path . $_FILES["image"]["name"]);
                    //$url = "../public/img/profile/$new_image_name";
                    if (rename($path . $_FILES['image']['name'], $path . $new_image_name) && $temp_image == true) {
                        echo"refresh";
                    }
                }
            }
        }
    }

}

?>
