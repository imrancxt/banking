<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of account
 *
 * @author imran.v2v
 */
class account extends CI_Controller {

    //put your code here
    function __construct() {
        session_start();
        if (!isset($_SESSION['admin'])) {
            exit();
        }
        parent::__construct();
        $this->load->library('view_page');
        $this->load->model('madmin');
        $this->load->library('fcommon');
    }

    public function index() {
        $query = "SELECT * FROM `account_head`";
        $data = $this->madmin->get_data($query, array('serial', 'type', 'acc_head'), array('serial', 'acc_type', 'acc_head'));
        //print_r($data);
        $this->view_page->load_page('v_add_account_type', $data);
    }

    public function update_account_head() {
        $this->input->post(NULL, TRUE);
        $data = $this->input->post();
        $this->db->update('account_head', array('acc_head' => $data['acc_head']), array('serial' => $data['serial']));
        echo"Account Head Updated!";
    }

    public function affair() {
        $date = date("Y-m-d");
        $data = $this->get_affair_data($date);
        $this->view_page->load_page('v_affair', $data);
    }

    public function filter_affair($date) {
        $data = $this->get_affair_data($date);
        $this->load->view('vadmin/ajax_code/v_filter_affair', $data);
    }

    private function get_affair_data($date) {

        //LIABILITY
        $query1 = "SELECT t1.account_name,(sum(t1.cr)-sum(t1.dr)) as balance,t1.acc_type,t1.date FROM `general_ledger` as t1 
                 where t1.acc_type='liability' and t1.date<='$date' group by t1.account_name";
        $data1 = $this->madmin->get_data($query1, array('laccount_name', 'lbalance'), array('account_name', 'balance'));

        ;
        //ASSET
        $query2 = "SELECT t1.account_name,(sum(t1.cr)-sum(t1.dr)) as balance,t1.acc_type,t1.date FROM `general_ledger` as t1 
                 where t1.acc_type='asset' and t1.date<='$date' group by t1.account_name";
        $data2 = $this->madmin->get_data($query2, array('aaccount_name', 'abalance'), array('account_name', 'balance'));

        $data = array_merge($data1, $data2);
        $cash_in_hand = $this->affair_get_cash_in_hand($date);
        if ($cash_in_hand < 0) {
            $cash_in_hand = -($cash_in_hand);
        }
        $data['cash_in_hand'] = $cash_in_hand;
        return $data;
    }

    private function get_cash_in_hand($date) {
        $query = "SELECT (sum(t1.cr)-sum(t1.dr)) as cash_in_hand FROM `general_ledger` as t1 where  t1.date<'$date'";
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                if ($row->cash_in_hand != NULL) {
                    return $row->cash_in_hand;
                } else {
                    return 0;
                }
            }
        }
    }
    
     private function affair_get_cash_in_hand($date) {
        $query = "SELECT (sum(t1.cr)-sum(t1.dr)) as cash_in_hand FROM `general_ledger` as t1 where  t1.date<='$date'";
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                if ($row->cash_in_hand != NULL) {
                    return $row->cash_in_hand;
                } else {
                    return 0;
                }
            }
        }
    }
    
    
     public function cash_in_hand_gl() {
       /* $query = "SELECT date, sum(cr) as received_from_client,sum(dr) 
            as paid_to_client,(sum(cr)-sum(dr)) as cash_in_hand  from `general_ledger`  group by date";
        $data = $this->madmin->get_data($query, array('date', 'received_from_client', 'paid_to_client', 'cash_in_hand_nr'), array('date', 'received_from_client', 'paid_to_client', 'cash_in_hand'));
       */
        $data=$this->this_month_cash_in_hand_gl(date("Y-m"));
        $this->view_page->load_page('v_cash_in_hand_gl', $data);
    }
    private function this_month_cash_in_hand_gl($year_month){
        $date1="$year_month-01";
        $query= "SELECT date, sum(cr) as received_from_client,sum(dr) 
            as paid_to_client,(sum(cr)-sum(dr)) as cash_in_hand  from `general_ledger` where left(date,7)='$year_month'  group by date";
        
        $data= $this->madmin->get_data($query, array('date', 'received_from_client', 'paid_to_client', 'cash_in_hand_nr'), array('date', 'received_from_client', 'paid_to_client', 'cash_in_hand'));
        $data['opening_amount']=$this->get_cash_in_hand($date1);
        $data["month"]=$year_month;
        return $data;
    }
    public function filter_cash_in_hand_gl(){
        $month=$_GET["month"];
        $data=$this->this_month_cash_in_hand_gl($month);
        $this->load->view("vadmin/common_page/v_filter_cash_in_hand_gl_data",$data);
       // print_r($data);
    }

    public function view_supplementary1() {
        $date = date("Y-m-d");
        //$date="2016-01-07";
        $data = $this->get_supplementary_data("S.B", 'dr', $date);
        //$this->load->view('vadmin/ajax_code/v_view_supplementary1', $data);
        $this->view_page->load_page('v_view_supplementary1',$data);
    }

    public function view_supplementary2(){
       $query="SELECT acc_head FROM `account_head`";
       $data=$this->madmin->get_data($query,array('acc_head'),array('acc_head'));
       $this->view_page->load_page('v_view_supplementary2',$data);
    }

    public function get_supplementary1_info($supplementary_name, $supplementary_type, $date) {
        $data = $this->get_supplementary_data($supplementary_name, $supplementary_type, $date);
        $this->load->view('vadmin/ajax_code/v_get_supplementary1_info', $data);
    }

    private function get_supplementary_data($supplementary_name, $supplementary_type, $date) {
        $table = $this->fcommon->get_account_type_table($supplementary_name);
        //print_r($table);

        $query1 = "SELECT t1.serial,t1.member_no,t2.name,t1.account_no,t1.cash,t1.transfer,t1.$supplementary_type,t1.comment FROM `{$table['ledger_account']}` as t1,member_info as t2 where t1.member_no=t2.member_no
                and t1.$supplementary_type>0 and t1.date='$date' and (t1.trans_type!='interest' && t1.trans_type!='service charge' && t1.trans_type!='book')";
       


        $data1 = $this->madmin->get_data($query1, array('serial', 'member_no', 'name', 'account_no', 'cash', 'transfer', 'atotal', 'comment'), array('serial', 'member_no', 'name', 'account_no', 'cash', 'transfer', $supplementary_type, 'comment'));

        $query2 = "SELECT sum(t1.$supplementary_type) as total FROM `general_ledger` as t1 where date='$date' and t1.$supplementary_type>0 and t1.account_name='$supplementary_name'";

        $data2 = $this->madmin->get_data($query2, array('total'), array('total'));

        $data = array_merge($data1, $data2);
        return $data;
    }

    public function change_account_status($account_type, $account_no, $status) {
        $table = $this->fcommon->get_account_type_table($account_type);
        if ($status == "current") {
            $status = "previous";
        } else {
            $status = "current";
        }
        $this->db->update($table['account_table'], array('status' => $status), array('account_number' => $account_no));
        echo $status;
    }

    public function cash_position_memo() {
        $date = date("Y-m-d");
        $cash_in_hand = $this->get_cash_in_hand($date);
        if ($cash_in_hand < 0) {
            $cash_in_hand = ($cash_in_hand);
        }
        $data1 = $this->get_cashmemo_drcr($date);
        $data1['opening_amount'] = $cash_in_hand;

        $this->view_page->load_page('v_cash_position_memo', $data1);
        //print_r($data1);
    }

    private function get_cashmemo_drcr($date) {
       /* $query = "SELECT sum(cr) as received_from_client,sum(dr) 
            as paid_to_client  from `general_ledger` where date='$date' and (account_name='S.B' or account_name='Loan')";*/
        $account_name="(account_name='Income A/c' or account_name='S.B' or account_name='Loan')";
        $query = "SELECT sum(cr) as received_from_client,sum(dr) 
            as paid_to_client  from `general_ledger` where date='$date'";// and $account_name;
        $data = $this->madmin->get_data($query, array('received_from_client', 'paid_to_client'), array('received_from_client', 'paid_to_client'));
        //echo $query;
        return $data;
    }

    public function filter_cash_position_memo($date) {
        $cash_in_hand = $this->get_cash_in_hand($date);
        if ($cash_in_hand < 0) {
            $cash_in_hand = ($cash_in_hand);
        }
        $data1 = $this->get_cashmemo_drcr($date);
        $data1['opening_amount'] = $cash_in_hand;

        $this->load->view('vadmin/ajax_code/v_filter_cash_position', $data1);
    }

    public function account_subhead() {
        $query1 = "SELECT * FROM `account_head`";
        $data1 = $this->madmin->get_data($query1, array('acc_head_serial', 'acc_head'), array('serial', 'acc_head'));
        $data2 = array();
        $query2 = "SELECT * FROM `acc_subhead`";
        $data2 = $this->madmin->get_data($query2, array('acc_subhead_serial', 'acc_subhead', 'acc_head2'), array('serial', 'acc_subhead', 'acc_head'));

        $data = array_merge($data1, $data2);
        $this->view_page->load_page('v_acc_subhead', $data);
    }

    public function get_subhead($head) {
        $query = "SELECT * FROM `acc_subhead` where acc_head='$head'";
        $data = $this->madmin->get_data($query, array('acc_subhead'), array('acc_subhead'));
        if (isset($data)) {
            foreach ($data['acc_subhead'] as $option) {
                echo"<option>$option</option>";
            }
        }
    }

    public function update_account_subhead() {
        $this->input->post(NULL, TRUE);
        $data = $this->input->post();
        $this->db->update('acc_subhead', array('acc_subhead' => $data['acc_subhead']), array('serial' => $data['serial']));
        echo"Account Head Updated!";
    }

    public function set_loan_profit_return() {
        $query1="SELECT * FROM `loan_account` where status='current'";
        $data1=$this->madmin->get_data($query1,array('acc_no'),array('account_number'));
        $this->view_page->load_page('v_set_loan_profit_return', $data1);
    }
    public function get_all_loan_inerest_value($account_no){
        $query="SELECT * FROM `set_loan_return` where loan_account_no='$account_no'";
        $data=$this->madmin->get_data($query,array('start_date','end_date','amount'),array('start_date','end_date','amount'));
        $this->load->view('vadmin/ajax_code/v_get_all_loan_inerest_value', $data);
    }
    public function test(){
        $this->view_page->test_page("v_open_loan_account","");
    }
    public function get_v_supplimentary2_data(){
        $acc_head=$_GET['acc_head'];
        $acc_subhead=$_GET['acc_subhead'];
        $date=$_GET["date"];
        $drcr=$_GET['drcr'];
        $query="SELECT * FROM `others_account_ledger` where acc_type='$acc_head' and date='$date' and acc_subhead='$acc_subhead' and `$drcr`>0";
        $data=$this->madmin->get_data($query,array('serial','dr','cr','comment','cash','transfer','particular'),array('serial','dr','cr','comment','cash','transfer','particular'));
        
        $this->load->view('vadmin/filter_get_v_supplimentary2_data',$data);
        //print_r($data);
    }
    

}

?>
