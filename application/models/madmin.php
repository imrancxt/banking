<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of madmin
 *
 * @author imran
 */
class madmin extends CI_Model {

    public function get_data($query, $var, $col) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                for ($j = 0; $j < count($var); $j++) {
                    $info[$var[$j]][$i] = $row->$col[$j];
                }
                $i++;
            }
        }
        return $info;
    }
    public function get_single_data($query,$col) {
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                return $row->$col;
            }
        } else {
            return 0;
        }
    }
    public function get_test_data(){
        $query="select data from `table`";
        $rs=$this->db->query($query);
        $data=array();
        $i=0;
        if($rs->num_rows()>0){
            foreach($rs->result() as $row){
                $data[$i]=$row->data;
                $i++;
            }
        }
        return $data;
    }
    

}

?>
