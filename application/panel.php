<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of panel
 *
 * @author imran.v2v
 */
class panel extends CI_Controller {
    //put your code here
    
    //put your code here
    function __construct() {
         session_start();
        if(!isset($_SESSION['admin'])){
            exit();
        }
        parent::__construct();
        //echo"test";
        $this->load->library('view_page');
    }

    function index() {
        $info['morris_data1'] = $this->get_account_info();
        $info['morris_data2'] = $this->get_account_drcr();
        $info['morris_data3'] = $this->individual_drcr("S.B");
        $info['morris_data4'] = $this->individual_drcr("Loan");
        // prin
        //print_r($info);SELECT date,sum(dr) as dr,sum(cr) as cr FROM `general_ledger` where account_name='S.B' group by date
        $this->view_page->admin_page('v_dash', $info);
    }

    private function individual_drcr($account_name) {
        $query = "SELECT date,sum(dr) as dr,sum(cr) as cr FROM `general_ledger` where account_name='$account_name' group by date";
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $data[$i] = array('y' => $row->date, 'a' => $row->dr, 'b' => $row->cr);
                $i++;
            }
        }
        return $data;
    }

    private function get_account_info() {
        $query = "SELECT count(member_no) as loan_account,(select count(member_no) from savings_account) as savings_account FROM `loan_account`";
        $label = array('Loan Account', 'Savings Account');
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row) {
                $data[0] = array('label' => $label[0], 'value' => $row->loan_account);
                $data[1] = array('label' => $label[1], 'value' => $row->savings_account);
            }
        }
        return $data;
    }

    private function get_account_drcr() {
        $query = "SELECT account_name,sum(dr) as dr,sum(cr) as cr FROM `general_ledger` group by account_name";
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $data[$i] = array('y' => $row->account_name, 'a' => $row->dr, 'b' => $row->cr);
                $i++;
            }
        }
        return $data;
    }

    public function add_member() {
        echo"test";
    }
}

?>
