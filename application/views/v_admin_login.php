
<html>
    <head>
        <meta charset="utf-8">
        <link href="<?php echo base_url() ?>dist/css/login.css" rel="stylesheet">
        <script src='<?php echo base_url() ?>dist/js/jquery.js'></script>
        <script src='<?php echo base_url() ?>dist/js/login.js'></script>
    </head>
    <body>
        <div class="login">
            <form id='login_form' action='<?php echo base_url() ?>login/check_admin' method='POST' enctype='multipart/form-data'> 
                <fieldset>
                    <legend> <h1><strong>Welcome </strong>Admin</h1></legend>
                    <p><select name="user_type" style="margin-left: .8px; height: 30px"><option>admin</option><option>sales</option><option>manager</option></select></p>
                    <p><input name="admin" type="text" required placeholde="Admin" onBlur="if(this.value=='')this.placeholder='Admin'" onFocus="if(this.placeholder=='Admin')this.placeholder='' "></p>
                    <p><input name="password" type="password" required placeholder="Password" onBlur="if(this.value=='')this.placeholder='Password'" onFocus="if(this.placeholder=='Password')this.placeholder='' "></p>
                    <p><input type="submit" value="Login"></p>
                </fieldset>
            </form>
        </div>    
    </body>
</html>