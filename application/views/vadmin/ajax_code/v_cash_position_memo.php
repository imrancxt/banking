
    <div class="row">

        <div class="col-lg-3">
            <div class="panel panel-teal">
                <div class="panel-heading">Filter Cash Position Memo</div>
                <div class="panel panel-body">
                    <input class="form-control" type="date" value="<?php echo date("Y-m-d"); ?>" id="date"/><br>
                    <button class="btn btn-default" style="width: 100%" id="filter_cash_position_memo">Filter</button>
                </div>
            </div>


        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-teal">
                <div class="panel-heading">
                    Cash Position Memo
                </div>
                <div class="panel-body">
                    <div id="cash_position_data">
                        <?php
                        $this->load->view('vadmin/ajax_code/v_filter_cash_position');
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>



<script>
    $(document).ready(function(){
        $("#filter_cash_position_memo").click(function(){
            date=$("#date").val();
            page="../account/filter_cash_position_memo/"+date;
            //alert(page);
            change_content("#cash_position_data",page);
        })
    })
</script>
