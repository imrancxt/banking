
                <div class="table-responsive">
                    <table class="table table-bordered table-hover print_table" id='ledger_info'>
                        <caption>
                             General Ledger of
                <?php echo $account_type; ?><hr>
                        </caption>
                        <thead>
                            <tr class="active">
                                <th>
                                    Date
                                </th>
                                <th>Particular</th>
                                <th>
                                    Debit
                                </th>
                                <th>
                                    Credit
                                </th>
                                <th>
                                    Balance
                                </th>
                                
                            </tr>
                                                    </thead>
                                                    <tbody>
                            <?php
                            $balance = $opening_balance;
                            if (isset($date)) {
                                for ($i = 0; $i < count($date); $i++) {
                                    $balance = $cr[$i] - $dr[$i] + $balance;
                                    echo"<tr class='info'><td>$date[$i]</td><td>$particular[$i]</td><td>$dr[$i]</td><td>$cr[$i]</td><td>$balance</td></tr>";
                                }
                            }
                            ?>
                            <tbody>
                    </table>
                </div>
          