<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-teal">
            <div class="panel-heading">
                Total User <strong id="row_counter">
                    <?php
                    if (isset($user_type)) {
                        echo count($user_type);
                    } else {
                        echo 0;
                    }
                    ?>
                   
                </strong>
            </div>
            <div class="panel-body">
                 <button class="btn btn-info" data-toggle='modal' data-target='#add_user_type_modal'>
                        Add New User
                    </button>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="data_table">
                        <thead>
                            <tr class='active'>
                                <th>Serial</th>
                                <th>Name</th>
                                <th>User Type</th>
                                <th>Password</th>
                                <th>Edit</th>
                                <th>Delete</th>

                            </tr>
                        </thead>
                        <?php
                        if (isset($user_type)) {
                            $j = 1;
                            for ($i = 0; $i < count($user_type); $i++) {
                                echo"<tr class='info' role='row'>
                                <td>$j</td><td>$name[$i]</td><td>$user_type[$i]</td><td>$password[$i]</td><td><button user_serial='$serial[$i]' user_name='$name[$i]' user_type='$user_type[$i]' password='$password[$i]' class='btn btn-success' data-toggle='modal' data-target='#edit_user_type_modal'>Edit</button></td>
                            <td><button class='btn btn-danger' user_serial='$serial[$i]'>Delete</button></td>
                            </tr>";
                                $j++;
                            }
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="edit_user_type_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">Edit User Info</h2>
            </div>
            <form class='change_content_by_refresh' action='../panel/update_user_info' method='POST' enctype='multipart/form-data'>
                <div class="modal-body" id="alert_modal_body">
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" id="user_name" name="name"/>
                        <input type="hidden" name='serial' id="user_serial"/>
                    </div>

                    <div class="form-group">
                        <label>User Type</label>
                        <select class="form-control" name="user_type">
                            <option>admin</option>
                            <option>sales</option>
                            <option>manager</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input name='password' type="password" class="form-control" id="user_password"/>
                    </div>


                </div>   
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>


<div class="modal fade" id="add_user_type_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">User Info</h2>
            </div>
            <form class='change_content_by_refresh' action='../panel/add_user_info' method='POST' enctype='multipart/form-data'>
                <div class="modal-body" id="alert_modal_body">
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" id="user_name" name="name"/>
                    </div>

                    <div class="form-group">
                        <label>User Type</label>
                        <select class="form-control" name="user_type">
                            <option>admin</option>
                            <option>sales</option>
                            <option>manager</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input name="password" type="password" class="form-control" id="user_password"/>
                    </div>


                </div>   
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".btn-success").click(function(){
            user_name=$(this).attr("user_name");
            user_password=$(this).attr('password');
            user_serial=$(this).attr("user_serial");
            
            $("#user_name").val(user_name);
            $("#user_password").val(user_password);
            $("#user_serial").val(user_serial);
            //change_content(this,page);
        });
        $(".btn-danger").click(function(){
            serial=$(this).attr("user_serial");
            page="../panel/delete_user?serial="+serial;
            page_refresh(page);
        })
    })
</script>