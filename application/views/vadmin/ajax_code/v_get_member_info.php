
<div class="table-responsive">
    <table class="table table-bordered table-hover">
        <tr class="info">
            <td>
                Account Number
            </td>
            <td>
                <input class="form-control" name="account_number" placeholder="Account Number" required>
                <input type="hidden" value="<?php echo $member_no[0] ?>" name="member_no"/>
            </td>
        </tr>
        <?php
        if(isset($account_type)){
            if($account_type=="savings_account"){
                include_once 'savings_account_amount.php';
            }
        }
        ?>
        <tr class="info">
            <td>Opening Date</td>
            <td>
                <input type="date" class="form-control" name="opening_date" required>
            </td>
        </tr>
        <tr class="info">
            <td>
                Member No
            </td>
            <td><?php echo $member_no[0] ?></td>
        </tr>
        <tr class="info">
            <td>
                Name
            </td>
            <td><?php echo $name[0] ?></td>
        </tr>
        <tr class="info">
            <td>
                Father Name
            </td>
            <td><?php echo $father_name[0] ?></td>
        </tr>
        <tr class="info">
            <td>
                Mother Name
            </td>
            <td><?php echo $mother_name[0] ?></td>
        </tr>
        <tr class="info">
            <td>
                Current Address
            </td>
            <td><?php echo $current_address[0] ?></td>
        </tr>
        <tr class="info">
            <td>
                Permanent Address
            </td>
            <td><?php echo $permanent_address[0] ?></td>
        </tr>
        <tr class="info">
            <td>
                Phone No
            </td>
            <td><?php echo $phone_no[0] ?></td>
        </tr>  
        <tr class="info">
            <td>
                Age
            </td>
            <td><?php echo $age[0] ?></td>
        </tr>  
        <tr class="info">
            <td>
                Nationality
            </td>
            <td><?php echo $nationality[0] ?></td>
        </tr>  
        <tr class="info">
            <td>
                Occupation
            </td>
            <td><?php echo $occupation[0] ?></td>
        </tr> 
        <tr class="info">
            <td>Comment</td>
            <td>
                <textarea class="form-control" name="comment" placeholder="Comment"></textarea>
            </td>
        </tr>
    </table>
    <button class="btn btn-info" type="submit" style="width:100%" data-toggle="modal" data-target="#alert_modal">Submit</button>
</div>