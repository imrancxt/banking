
    <div class="row">
        <div class="col-lg-12">
        <div class="panel panel-teal">
            <div class="panel-heading">
                Add Account Head
            </div>
            <div class="panel-body">
                <form style="border: none"  class='change_content_by_alert' action='../upload/add_acc_type' method='POST' enctype='multipart/form-data'>
            <div class="col-lg-4">
                <label>
                    Select Account Type
                </label>
                <select class="form-control" name="acc_type">
                    <?php
                    $acc_type = array('asset', 'liability');
                    foreach ($acc_type as $option) {
                        echo"<option>$option</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-4">
                <label>Account Head</label>
                <input class="form-control" name="acc_head" required placeholder="Account Head"/>
            </div>
            <div class="col-lg-4">
                <label>Submit</label><br>
                <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#alert_modal" style="width:100%">Submit</button>
            </div>
            
        </form>
            </div>
        </div>
        </div>
        
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-teal">
                <div class="panel-heading">
                    All account Head
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                <table class="table table-bordered table-hover" id="data_table">
                    <thead>
                        <tr class="active"> 
                            <th>
                                Serial
                            </th>
                            <th>
                                Account Type
                            </th>
                            <th>
                                Account Head
                            </th>
                            <th>
                                Edit
                            </th>
                        </tr>
                    </thead>
                    <?php
                    if (isset($serial)) {
                        $j = 0;
                        for ($i = 0; $i < count($serial); $i++) {
                            $j++;
                            echo"<tr class='info'><td>$j</td><td>$type[$i]</td><td>$acc_head[$i]</td>
                            <td><butlton class='btn btn-info' serial='$serial[$i]' acc_head='$acc_head[$i]' data-toggle='modal' data-target='#alert_modal2'>Edit</button></td>
                            </tr>";
                        }
                    }
                    ?>
                </table>
            </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="modal fade" id="alert_modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <form style="border: none" class='change_content_by_alert2' action='../account/update_account_head' method='POST' enctype='multipart/form-data'>
                    <div class="modal-body" id="" style="margin-bottom: 0px !important">
                        <label>
                            Account Head
                        </label>
                        <input type="hidden" name="serial" id="acc_serial"/>
                        <input class="form-control" name="acc_head" id="acc_head"/>
                        <button type="submit" class="btn btn-primary" style="width:100%">Update</button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
<?php
            include_once 'alert_modal.php';
            ?>
<script>
    $(document).ready(function(){
        $(".btn-info").click(function(){
            serial=$(this).attr("serial");
            acc_head=$(this).attr("acc_head");
            $("#acc_head").val(acc_head);
            $("#acc_serial").val(serial);
        })
    })
</script>