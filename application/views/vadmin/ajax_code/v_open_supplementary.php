
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-teal">
                <div class="panel-heading">
                    Supplementary1
                </div>
                <div class="panel-body">
                    <form style="border: none" content="alert_box1" class='change_content_by_alert' action='../upload/add_supplementary_data' method='POST' enctype='multipart/form-data'>
            <div class="col-lg-2">
                Date:<input class="form-control" name="date" type="date" required value="<?php echo date("Y-m-d") ?>"/>
            </div>
            <div class="col-lg-2">
                Supplementary Name:
                <select class="form-control" id="supplementary_name">
                    <?php
                    $supllementary_name = array('Loan','S.B');
                    foreach ($supllementary_name as $option) {
                        echo"<option>$option</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-2">
                Supplementary Type:
                <select class="form-control" name="supplementary_type">
                    <?php
                    $supllementary_type = array('dr', 'cr');
                    foreach ($supllementary_type as $option) {
                        echo"<option>$option</option>";
                    }
                    ?>
                </select>
            </div>

            <div class="col-lg-2">
                Transaction Type:
                <select class="form-control" name="trans_type">
                    <?php
                    $option=array('nett','interest','service charge','book');
                    foreach ($option as $value) {
                        echo"<option>$value</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-2">
                Check:<br>
                <a class="btn btn-info" id="check_supplementary_account">Accounts</a>
            </div>
            <div class="col-lg-2">
                Submit<br>
                <button class="btn btn-primary" type="submit" data-toggle="modal" data-target="#alert_modal">Submit</button>
            </div>
            <div class="col-lg-12">
                <div class="table-responsive" id="supplementary_account">

                </div>
            </div>
        </form>
        
                </div>
            </div>
        </div>
        
    </div>
<?php
        include_once 'alert_modal.php';
        ?>
<script>
    $(document).ready(function(){
        $("#check_supplementary_account").click(function(){
            supplementary_name=$("#supplementary_name").val();
            page="../member/get_account_info/"+supplementary_name;
            change_content("#supplementary_account",page);
        })
    })
</script>