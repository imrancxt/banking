
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-teal">
            <div class="panel-heading">
                Add Account Sub-Head
            </div>
            <div class="panel-body">
                <form style="border: none" content="alert_box1" class='change_content_by_alert' action='../upload/add_acc_subhead' method='POST' enctype='multipart/form-data'>
                    <div class="col-lg-4">
                        <label>
                            Select Account Head
                        </label>
                        <select class="form-control" name="acc_head">
                            <?php
                            if (isset($acc_head)) {
                                $i = 0;
                                foreach ($acc_head as $option) {
                                    echo"<option>$option</option>";
                                    $i++;
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label>Account Sub-Head</label>
                        <input class="form-control" name="acc_subhead" required placeholder="Account Sub-Head"/>
                    </div>
                    <div class="col-lg-4">
                        <label>Submit</label><br>
                        <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#alert_modal" style="width:100%">Submit</button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-teal">
            <div class="panel-heading">
                All Sub-Head
            </div>
            <div class="panel-body">
                <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data_table">
                <thead>
                    <tr class="active"> 
                        <th>
                            Serial
                        </th>
                        <th>
                            Account Sub-Head
                        </th>
                        <th>
                            Account Head
                        </th>
                        <th>
                            Edit
                        </th>
                    </tr>
                </thead>
                <?php
                if (isset($acc_subhead_serial)) {
                    $j = 0;
                    for ($i = 0; $i < count($acc_subhead_serial); $i++) {
                        $j++;
                        echo"<tr class='info'><td>$j</td><td>$acc_subhead[$i]</td><td>$acc_head2[$i]</td>
                            <td><butlton class='btn btn-info' serial='$acc_subhead_serial[$i]' acc_subhead='$acc_subhead[$i]' data-toggle='modal' data-target='#alert_modal2'>Edit</button></td>
                            </tr>";
                    }
                }
                ?>
            </table>
        </div>
            </div>
        </div>
        
    </div>
</div>

<div class="modal fade" id="alert_modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form style="border: none" class='change_content_by_alert2' action='../account/update_account_subhead' method='POST' enctype='multipart/form-data'>
                <div class="modal-body"  style="margin-bottom: 0px !important">
                    <label>
                        Account Head
                    </label>
                    <input type="hidden" name="serial" id="acc_serial"/>
                    <input class="form-control" name="acc_subhead" id="acc_subhead"/><br>
                    <button type="submit" class="btn btn-primary" style="width:100%">Update</button>
                </div>

            </form>
        </div>
    </div>
</div>
<?php
                    include_once 'alert_modal.php';
                    ?>
<script>
    $(document).ready(function(){
        $(".btn-info").click(function(){
            serial=$(this).attr("serial");
            acc_subhead=$(this).attr("acc_subhead");
            $("#acc_subhead").val(acc_subhead);
            $("#acc_serial").val(serial);
        })
    })
</script>