
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-teal">
                <div class="panel-heading">
                    Supplementary2
                </div>
                <div class="panel-body">
                     <form style="border: none" content="alert_box1" class='change_content_by_alert' action='../upload/add_supplementary_data2' method='POST' enctype='multipart/form-data'>
            <div class="col-lg-2">
                Date:<input class="form-control" name="date" type="date" value="<?php echo date("Y-m-d") ?>" required/>
            </div>
            <div class="col-lg-2">
                Supplementary Name:
                <select class="form-control" id="supplementary_name" name="supplementary_name">
                    <?php
                    $supllementary_name=$acc_head;
                    foreach ($supllementary_name as $option) {
                        echo"<option>$option</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-2">
                Account Sub-Head
                <select required class="form-control" name="acc_subhead">
                    <?php
                    if(isset($acc_subhead)){
                        foreach($acc_subhead as $option){
                            echo"<option>$option</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-2">
                Supplementary Type:
                <select class="form-control" name="drcr">
                    <?php
                    $supllementary_type = array('dr', 'cr');
                    foreach ($supllementary_type as $option) {
                        echo"<option>$option</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-2">
                Cash
                <input type="number" class="form-control" name="cash" placeholder="Cash" value="0"/>
            </div>
            <div class="col-lg-2">
                Transfer
                <input type="number" class="form-control" name="transfer" placeholder="Transfer" value="0"/>
            </div>
            <div class="col-lg-2">
                Comment
                <input class="form-control" name="comment" placeholder="Comment"/>
            </div>
            <div class="col-lg-2">
                <br>
                <button class="btn btn-primary" type="submit" data-toggle="modal" data-target="#alert_modal">Submit</button>
            </div>
        </form>
        
                </div>
            </div>
        </div>
       
    </div>
<?php
        include_once 'alert_modal.php';
        ?>
<script>
    $(document).ready(function(){
        $("#check_supplementary_account").click(function(){
            supplementary_name=$("#supplementary_name").val();
            page="../member/get_account_info/"+supplementary_name;
            change_content("#supplementary_account",page);
        })
        $("#supplementary_name").on('change',function(){
            val=$(this).val();
            page="../account/get_subhead/"+val;
            change_content("select[name='acc_subhead']",page);
        })
    })
</script>