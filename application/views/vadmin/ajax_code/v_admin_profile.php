<div class="row">
    <div class="col-lg-6 col-md-offset-3">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Edit Information
            </div>
            <form class='change_content_by_refresh' action='../panel/update_bank_name' method='POST' enctype='multipart/form-data'>
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Somobai Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $_SESSION['bank_name'] ?>" name="bank_name">
                        </div>
                    </div><br>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Address</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $_SESSION['address'] ?>" name="address">
                        </div>
                    </div><br>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Phone Number</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="<?php echo $_SESSION['phone_no'] ?>" name="phone_no">
                        </div>
                    </div><br>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" name="password">
                        </div>
                    </div>
                    <br><br>
                    <div class="col-sm-8 col-sm-offset-4">
                        <button type="submit" class="btn-success btn" style="width:100%">Update Info</button>

                    </div>

                    <div class="col-sm-8 col-sm-offset-4">
                        <br>
                        <button type="button" class="btn btn-info" style="width:48.5%">Change Admin Info</button>
                        <button type="button" class="btn btn-info" style="width:50%">Change Avatar</button>
                        
                    </div>

                </div>
            </form>
        </div>
    </div>    
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="modal fade" id="change_password_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Change Admin Info</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="remove_asset_serial"/>
                        Are You Sure To Remove This Asset?
                    </div>   

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <a class="btn btn-primary" href="javascript:delete_asset()">Yes</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>