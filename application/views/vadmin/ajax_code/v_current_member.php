
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-teal">
                <div class="panel-heading">
                    Current Member <strong id="row_counter">
            <?php if(isset($member_no)){
                echo count($member_no);
            }
            else{
                echo 0;
            }
            ?>
        </strong>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                <table class="table table-bordered table-hover" id="data_table">
                    <thead>
                        <tr class='active'>
                            <th>Member No</th>
                            <th>Name</th>
                            <th>Father's Name</th>
                            <th>Mother's Name</th>
                            <th>Current Address</th>
                            <th>Phone No</th>
                            <th>Opening Date</th>
                            <th>Occupation</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <?php
                    if(isset($member_no)){
                        for($i=0;$i<count($member_no);$i++){
                            echo"<tr class='info' role='row'><td><a style='width:100%' class='btn btn-success' href='../member/profile?id=$member_no[$i]'>$member_no[$i]</a></td><td>$name[$i]</td><td>$father_name[$i]</td><td>$mother_name[$i]</td>
                            <td>$current_address[$i]</td><td>$phone_no[$i]</td><td>$opening_date[$i]</td><td>$occupation[$i]</td>
                            <td><button class='btn btn-primary' member_no='$member_no[$i]'>Current</button></td>
                            </tr>";
                        }
                    }
                    ?>
                </table>
            </div>
                </div>
            </div>
            
        </div>
    </div>
<script>
    $(document).ready(function(){
        $(".btn-primary").click(function(){
            member_no=$(this).attr("member_no");            
            page="../member/deactive_member/"+member_no;
            
            change_content(this,page);
        })
    })
    </script>