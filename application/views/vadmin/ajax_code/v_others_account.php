
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-teal">
                <div class="panel-heading">
                     Other account head 
                </div>
                <div class="panel-body">
                     <div class="col-lg-3">
            Select account head:
            <select class="form-control" id="acc_head">
                <?php
                if(isset($acc_head)){
                    foreach ($acc_head as $option) {
                        echo"<option>$option</option>";
                    }
                }
                ?>
            </select>
        </div>
            <div class='col-lg-3'>
            Select month:
            <input class='form-control' type='month' id='month'/>
            </div>
            <div class='col-lg-2'>
                Check:
            <button class="btn btn-primary" id="check_head_ledger" style="width:100%">
                Check
            </button>
        </div>
        <div class='col-lg-3'>
            Print:
             <button class='btn btn-info btn-print_this_page' content="#ledger_data" style="width:100%">Print</button>
        </div>

        <div class="col-md-12" id="ledger_data">

        </div>
                </div>
            </div>
        </div>
       
    </div>

<script>
    $(document).ready(function(){
        $("#check_head_ledger").click(function(){
            acc_head=$("#acc_head").val();
            month=$("#month").val();
            page="../transaction/get_others_account_ledger_data?acc_head="+acc_head+"&month="+month;
            change_content("#ledger_data",page);
            //change_alert(page);
            
        })
    })
</script>