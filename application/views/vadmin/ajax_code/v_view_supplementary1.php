
    <div class="row">
        <div class="col col-lg-12">
            <div class="panel panel-teal">
                <div class="panel-heading">
                    View Supplementary1
                </div>
                <div class="panel-body">
                    <div class="col-lg-3">
                Date:<input class="form-control" name="date" type="date" id="date" value="<?php echo date('Y-m-d'); ?>"/>
            </div>
            <div class="col-lg-3">
                Supplementary Name:
                <select class="form-control" id="supplementary_name">
                    <?php
                    $supllementary_name = array('S.B', 'Loan');
                    foreach ($supllementary_name as $option) {
                        echo"<option>$option</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-2">
                Supplementary Type:
                <select class="form-control" name="supplementary_type" id="supplementary_type">
                    <?php
                    $supllementary_type = array('dr', 'cr');
                    foreach ($supllementary_type as $option) {
                        echo"<option>$option</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-lg-2">
                Check:<br>
                <a class="btn btn-info" id="check_supplementary_account">Accounts</a>
            </div>
            <div class="col-lg-12"><br>
                <div class="table-responsive" id="supplementary_account">
                    <?php
                    $this->load->view('vadmin/ajax_code/v_get_supplementary1_info');
                    ?>
                </div>
            </div>
                </div>
            </div>
        </div>
            
    </div>

<script>
    $(document).ready(function(){
        $("#check_supplementary_account").click(function(){
            supplementary_name=$("#supplementary_name").val();
            date=$("#date").val();
            supplementary_type=$("#supplementary_type").val();
            page="../account/get_supplementary1_info/"+supplementary_name+"/"+supplementary_type+"/"+date;
            //alert(page);
            change_content("#supplementary_account",page);
        })
    })
</script>