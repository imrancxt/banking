<?php
//array('member_no', 'name', 'father_name', 'mother_name', 
//'current_address', 'permanent_address', 'phone_no', 'age', 'nationality', 'occupation','profile_img','signature_img')
?>
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-teal">
            <div class="panel-heading">
                Basic Information
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>Member No:<?php echo $member_no[0] ?></label>
                </div>
                <div class="form-group">
                    <label>Name:<?php echo $name[0] ?></label>
                </div>
                <div class="form-group">
                    <label>Father's Name:<?php echo $father_name[0] ?></label>
                </div>
                 <div class="form-group">
                    <label>Mother's Name:<?php echo $mother_name[0] ?></label>
                </div>
                <div class="form-group">
                    <label>Current Address:<?php echo $current_address[0] ?></label>
                </div>
                <div class="form-group">
                    <label>Phone No:<?php echo $phone_no[0] ?></label>
                </div>
                <div class="form-group">
                    <label>Age:<?php echo $age[0] ?></label>
                </div>
              
                <div class="form-group">
                    <label>Nationality:<?php echo $nationality[0] ?></label>
                </div>
                <div class="form-group">
                    <label>Occupation:<?php echo $occupation[0] ?></label>
                </div>
            </div>
        </div>
    </div>
    <div class='col-lg-4'>
        <div class="panel panel-teal">
            <div class="panel-heading">
                All Account Info
            </div>
            <div class='panel-body'>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="data_table">
                       <thead>
                        <tr>
                           <th>
                            Account Type
                           </th> 
                           <th>
                            Account Number
                           </th>
                           <th>Opening Date</th>
                        </tr>
                       </thead>
                       <?php
                       if(isset($loan_acc_number)){
                        $i=0;
                        foreach($loan_acc_number as $inf){
                            echo"<tr class='info'><td>Loan</td><td>$inf</td><td>$loan_opening_date[$i]</td></tr>";
                            $i++;
                        }
                       }

                       if(isset($savings_acc_number)){
                        $i=0;
                        foreach($savings_acc_number as $inf){
                            echo"<tr class='info'><td>Savings</td><td>$inf</td><td>$savings_opening_date[$i]</td></tr>";
                            $i++;
                        }
                       }
                       ?>
                    </table>
                    </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-teal">
            <div class="panel-heading">
                Profile Picture
            </div>
            <div class="panel-body">
                <img class="img-square" src="../img/profile/<?php echo $profile_img[0] ?>" alt="" style="height: 140px; width: 180px">
            </div>
            <div class="panel-footer">
                <button class="btn btn-info" data-toggle="modal" data-target="#profile_modal">
                    Update Profile Picture
                </button>
            </div>
        </div>
        <div class="panel panel-teal">
            <div class="panel-heading">
                Signature Picture
            </div>
            <div class="panel-body">
                <img class="img-square" src="../img/signature/<?php echo $signature_img[0] ?>" alt="" style="height: 143px; width: 200px">
            </div>
            <div class="panel-footer">
                <button class="btn btn-info" data-toggle="modal" data-target="#signature_modal">
                    Update Signature Picture
                </button>
            </div>
        </div>

        
    </div>
    <div class="col-lg-3">
        
    </div>
</div>

<div class="modal fade" id="profile_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form style="border: none" content="this"  class='change_content_by_refresh' action='../upload/update_profile_pic' method='POST' enctype='multipart/form-data'>
                <div class="modal-body">
                    <img class="img-square" src="../img/signature/<?php echo $profile_img[0] ?>" alt="" style="height: 170px; width:100%">
                    <input class="btn btn-default" type="file" name="image" style="width:100%">
                    <input name="member_id" type="hidden" value="<?php echo $member_no[0] ?>"/>
                </div>   
                <div class="modal-footer">
                    <button class="btn btn-default" type="submit">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </form>

        </div>
    </div>
</div>


<div class="modal fade" id="signature_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form style="border: none" content="this"  class='change_content_by_refresh' action='../upload/update_signature_pic' method='POST' enctype='multipart/form-data'>
                <div class="modal-body">
                    <img class="img-square" src="../img/signature/<?php echo $signature_img[0] ?>" alt="" style="height: 170px; width:100%">
                    <input class="btn btn-default" type="file" name="image" style="width:100%">
                    <input name="member_id" type="hidden" value="<?php echo $member_no[0] ?>"/>
                </div>   
                <div class="modal-footer">
                    <button class="btn btn-default" type="submit">Update</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </form>

        </div>
    </div>
</div>