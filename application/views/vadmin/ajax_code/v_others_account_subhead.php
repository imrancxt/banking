
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-teal">
            <div class="panel-heading">
                Other account Subhead 
            </div>
            <div class="panel-body">
                <div class="col-lg-2">
                    <div class="form-group">
                        Select account head:
                        <select class="form-control" id="acc_head">
                            <option>Select</option>
                            <?php
                            if (isset($acc_head)) {
                                foreach ($acc_head as $option) {
                                    echo"<option>$option</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class='col-lg-2'>
                    <div class="form-group">
                        Select account subhead:
                        <select class="form-control" id="acc_sub_head">
                            <option>Select</option>
                        </select>
                    </div>
                </div>
                <div class='col-lg-2'>
                     Select month:
            <input class='form-control' type='month' id='month'/>
            </div>
            <div class='col-lg-2'>
                    <div class="form-group">
                        Check
                        <button class="btn btn-info" id="check_sub_head_data" style="width: 100%">
                            Check
                        </button>
                         
                    </div>

                </div>
                <div class=col-lg-2>
                    Print
                    <button class='btn btn-info btn-print_this_page' content="#ledger_data" style="width:100%">Print</button>
                </div>

                <div class="col-md-12" id="ledger_data">

                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        $("#acc_head").on("change",function(){
            acc_head=$(this).val();
            page="../transaction/get_acc_sub_head?acc_head="+acc_head;
            change_content("#acc_sub_head",page);
            //alert(page);
            
        });
        $("#check_sub_head_data").click(function(){
            acc_head=$("#acc_head").val();
            acc_subhead=$("#acc_sub_head").val();
            month=$("#month").val();
            page="../transaction/get_acc_subhead_data?acc_head="+acc_head+"&acc_subhead="+acc_subhead+"&month="+month;
            //alert(page);
            change_content("#ledger_data",page);
        })
    })
</script>