<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-teal">
            <div class="panel-heading">
                Cash In Hand General Ledger
            </div>
            <div class="panel-body">
                <div class="form-inline">
                    Select Month: <input class="form-control" id="month" type="month" value="<?php echo date("Y-m") ?>"/> <button id="check_month" class="btn btn-info">Check</button>
                </div><hr>
                <div class="table-responsive" id="data_table">
                    <?php
                    $this->load->view("vadmin/common_page/v_filter_cash_in_hand_gl_data");
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    $(document).ready(function(){
        $("#check_month").click(function(){
            month=$("#month").val();
            page="../account/filter_cash_in_hand_gl?month="+month;
            change_content("#data_table",page);
        })
    })
</script>