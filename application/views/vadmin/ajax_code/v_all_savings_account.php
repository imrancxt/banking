
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-teal">
                <div class="panel-heading">
                    All Savings Account
                    <strong id="row_counter">
                        <?php
                        if (isset($member_no)) {
                            echo count($member_no);
                        } else {
                            echo 0;
                        }
                        ?>
                    </strong>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="data_table">
                            <thead>
                                <tr class="active">
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Member No
                                    </th>
                                    <th>
                                        Account No
                                    </th>
                                    <th>
                                        Dr
                                    </th>
                                    <th>
                                        Cr
                                    </th>
                                    <th>
                                        Opening Amount
                                    </th>
                                    <th>
                                        Balance
                                    </th>
                                    <th>
                                        Opening Date
                                    </th>
                                    <th>
                                        Details
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                </tr>
                            </thead>
                            <?php
                            // array('name','member_no','account_no','dr','cr','opening_amount','opening_date')
                            $balance = 0;
                            if (isset($account_no)) {
                                for ($i = 0; $i < count($account_no); $i++) {
                                    $balance = $cr[$i] - $dr[$i]; //+$balance;
                                    echo"<tr class='info'><td>$name[$i]</td><td><a style='width:100%' class='btn btn-success' href='../member/profile?id=$member_no[$i]'>$member_no[$i]</a></td><td>$account_no[$i]</td>
                            <td>$dr[$i]</td><td>$cr[$i]</td><td>$opening_amount[$i]</td><td>$balance</td><td>$opening_date[$i]</td><td><a class='btn btn-primary' href='../transaction/get_indivisual_account?account_type=S.B&account_no=$account_no[$i]'>Details</a></td>
                            <td><button style='width:80px' class='btn btn-info' account_no='$account_no[$i]'>$status[$i]</button></td></tr>";
                                }
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
<script>
    $(document).ready(function(){
        $(".btn-info").click(function(){
            status=$(this).html();
            account_no=$(this).attr("account_no");
            page="../account/change_account_status/S.B/"+account_no+"/"+status;
            change_content(this,page);
        })
    })
</script>
