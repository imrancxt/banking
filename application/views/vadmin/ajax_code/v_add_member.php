
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-teal">
            <div class="panel-heading">
                Add New Member
            </div>
            <div class="panel-body">
                <form style="border: none" content="alert_box1" class='change_content_by_alert' action='../upload/add_member' method='POST' enctype='multipart/form-data'>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tr class="info">
                                <td>
                                    Name
                                </td>
                                <td>
                                    <input class="form-control" name="name" placeholder="Name"/>
                                </td>
                                <td>
                                    Father's Name
                                </td>
                                <td>
                                    <input class="form-control" name="father_name" placeholder="Father's Name"/>
                                </td>
                            </tr>
                            <tr class="info">
                                <td>
                                    Mother's Name
                                </td>
                                <td>
                                    <input class="form-control" name="mother_name" placeholder="Mother's Name"/>
                                </td>
                                <td>
                                    Current Address
                                </td>
                                <td>
                                    <textarea class="form-control" name="current_address" placeholder="Current Address"></textarea>
                                </td>
                            </tr>
                            <tr class="info">
                                <td>
                                    Permanent Address
                                </td>
                                <td>
                                    <textarea class="form-control" name="permanent_address" placeholder="Permanent Address"></textarea>
                                </td>
                                <td>
                                    Opening Date
                                </td>
                                <td>
                                    <input class="form-control" name="opening_date" type="date" required/>
                                </td>

                            </tr>
                            <tr class="info">
                                <td>
                                    Birth Date
                                </td>
                                <td>
                                    <input class="form-control" name="birth_date" type="date" required/>
                                </td>
                                <td>
                                    Age
                                </td>
                                <td>
                                    <input class="form-control" name="age" placeholder="Age"/>
                                </td>
                            </tr>
                            <tr class="info">
                                <td>
                                    Nationality
                                </td>
                                <td>
                                    <select class="form-control" name="nationality">
                                        <?php
                                        $nationality = array('Bangladeshi');
                                        foreach ($nationality as $option) {
                                            echo"<option>$option</option>";
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    Religion
                                </td>
                                <td>
                                    <select class="form-control" name="religion">
                                        <?php
                                        $religion = array('Islam', 'Hindu', 'Cristian');
                                        foreach ($religion as $option) {
                                            echo"<option>$option</option>";
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr class="info">
                                <td>Occupation</td>
                                <td><input class="form-control" name="occupation" placeholder="Ocupation"/></td>
                                <td>Education</td>
                                <td><input class="form-control" name="education" placeholder="Education"/></td>
                            </tr>
                            <tr class="info">
                                <td>Phone No</td>
                                <td><input class="form-control" name="phone_no" placeholder="Phone Number"/></td>
                                <td>Member No</td>
                                <td><input class="form-control" name="member_no" placeholder="Member No" required/></td>
                            </tr>
                            <tr>
                                <td>

                                </td>
                                <td>
                                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#alert_modal">Submit</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                   

                </form>
            </div>
        </div>

    </div>
</div>
 <?php
                    include_once 'alert_modal.php';
                    ?>
