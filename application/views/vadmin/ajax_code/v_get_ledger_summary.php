
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-teal">
            <div class="panel-heading">
                <?php
                if ($trans_type == 'dr') {
                    $trans_type1 = "Debit";
                } else {
                    $trans_type1 = "Credit";
                }
                echo $trans_type1
                ?> Ledger Summary
                <input type="hidden" id="trans_type" value="<?php echo $trans_type ?>"/>
            </div>
            <div class="panel-body">
                <div class="col-lg-2">
            <input class="form-control" type="date" id="date" value="<?php echo date("Y-m-d") ?>"/>
            <button class="btn btn-info" style="width:100%" id="filter_summary">Filter</button>
        </div>

        <div class="col-lg-12">
            <div class="table-responsive"  id="summary_content">
                <?php
                include_once 'summary_content.php';
                ?>
            </div>
        </div>
            </div>
        </div>
        </div>
        
        
    </div>
<script>
    $(document).ready(function(){
        $("#filter_summary").click(function(){
            date=$("#date").val();
            trans_type=$("#trans_type").val();
            page="../transaction/filter_summary/"+trans_type+"/"+date;
            
            change_content("#summary_content",page);
        })
    })
</script>