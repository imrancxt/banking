<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-teal">
            <div class="panel-heading">
                Ledger Data
            </div>
            <div class="panel-body">
                <div class='form-inline'><input id='month' type='month' class='form-control' value="<?php echo date("Y-m") ?>"/>
                <button class='btn tb-dedault' acc_type="<?php echo $account_type2; ?>" id='filter_ledger'>Filter</button>
                <button class='btn btn-info btn-print_this_page' content="#print_content">Print</button>
            </div>
            <div id="print_content">
                <?php
                $this->load->view('vadmin/ajax_code/v_get_general_ledger_data');
                ?>
            </div>
        </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    $("#filter_ledger").click(function(){
        acc_type=$(this).attr("acc_type");
        month=$("#month").val();
        page="../transaction/filter_get_general_ledger_data?month="+month+"&account="+acc_type;
        change_content("#ledger_info",page);
        //alert(page);
    })
})
</script>