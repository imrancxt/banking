<div class="col-lg-6">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="table1">
            <thead>
                <tr class="active">
                    <th>
                        Liabilities
                    </th>
                    <th>
                        Amount
                    </th>
                </tr>
            </thead>
            <?php
            $tlbalance = 0;
            if (isset($laccount_name)) {
                for ($i = 0; $i < count($laccount_name); $i++) {
                    if ($lbalance[$i] < 0) {
                        $lbalance[$i] = -($lbalance[$i]);
                    }
                    echo"<tr class='info'><td>$laccount_name[$i]</td><td>$lbalance[$i]</td></tr>";
                    $tlbalance+=$lbalance[$i];
                }
            }
            ?>
        </table>
    </div>
</div>
<div class="col-lg-6">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="table2">
            <thead>
                <tr class="active">
                    <th>
                        Assets
                    </th>
                    <th>
                        Amount
                    </th>
                </tr>
            </thead>
            <tr class="info">
                <td>
                    CASH IN HAND
                </td>
                <td>
                    <?php
                    echo $cash_in_hand;
                    ?>
                </td>
            </tr>
            <?php
            $tabalance =$cash_in_hand;
            if (isset($aaccount_name)) {
                for ($i = 0; $i < count($aaccount_name); $i++) {
                    if ($abalance[$i] < 0) {
                        $abalance[$i] = -($abalance[$i]);
                    }
                    echo"<tr class='info'><td>$aaccount_name[$i]</td><td>$abalance[$i]</td></tr>";
                    $tabalance+=$abalance[$i];
                }
            }
            ?>
        </table>
    </div>
</div>
<script>
    $(document).ready(function(){
        tlbalance=<?php echo $tlbalance ?>;
        tabalance=<?php echo $tabalance ?>;
        b1=parseInt(tlbalance);b2=parseInt(tabalance);
        b=0;
        /*if(b1>b2){
            b=b1-b2;
            b2+=b;
            html="<tr class='warning'><td>PROFIT AND LOSS A/C</td><td>"+b+"</td></tr>";
            $("#table2").append(html);
        }
        else{
            b=b2-b1;
            b1+=b;
            html="<tr class='warning'><td>PROFIT AND LOSS A/C</td><td>"+b+"</td></tr>";
            $("#table1").append(html);
        }*/
        html="<tr class='info'><td><strong>TOTAL<strong></td><td><strong>"+b1+"</strong></td></tr>";
        $("#table1").append(html);
        html="<tr class='info'><td><strong>TOTAL</td><td><strong>"+b2+"</strong></td></tr>";
        $("#table2").append(html);
        
    })
</script>