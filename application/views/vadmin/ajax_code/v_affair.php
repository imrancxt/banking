
<div class="row">
    <div class="col-lg-3">
        <div class="panel panel-teal">
            <div class="panel-heading">
                Filter Affair Data
            </div>
            <div class="panel-body">
                <input class="form-control" type="date" value="<?php echo date("Y-m-d"); ?>" id="date"/><br>
        <button class="btn btn-default" style="width: 100%" id="filter_affair">Filter</button>
            </div>
        </div>
        
    </div>
    <div class="col-lg-12">
        <div class="panel panel-teal">
            <div class="panel-heading">
                All Affair Data
            </div>
            <div class="panel-body">

                <div id="affair_data">
                    <?php
                    $this->load->view('vadmin/ajax_code/v_filter_affair');
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    $(document).ready(function(){
        $("#filter_affair").click(function(){
            date=$("#date").val();
            page="../account/filter_affair/"+date;
            //alert(page);
            change_content("#affair_data",page);
        })
    })
</script>
