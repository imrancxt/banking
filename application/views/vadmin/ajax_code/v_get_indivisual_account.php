
<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-teal">
            <div class="panel-heading">
                <?php echo $account_type; ?> Account of <?php echo $name[0] ?>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <tr class="info"><td>Name</td><td><?php echo $name[0] ?></td></tr>
                        <tr class="info"><td>Member No</td><td><?php echo $member_no[0] ?></td></tr>
                        <tr class="info"><td>Account Number</td><td><?php echo $account_number[0] ?></td></tr>
                        <?php
                        if (isset($opening_amount[0])) {
                            echo"<tr class='info'><td>Opening Amount</td><td>$opening_amount[0]</td></tr>";
                        }
                        ?>

                        <tr class="info"><td>Membership Date</td><td><?php echo $opening_date[0] ?></td></tr>
                        <tr class="info"><td>Opening Account Date</td><td><?php echo $sb_opening_date[0] ?></td></tr>
                        <tr class="info"><td>Father Name</td><td><?php echo $father_name[0] ?></td></tr>
                        <tr class="info"><td>Mother Name</td><td><?php echo $mother_name[0] ?></td></tr>
                        <tr class="info"><td>Current Address</td><td><?php echo $current_address[0] ?></td></tr>
                        <tr class="info"><td>Permanent Address</td><td><?php echo $permanent_address[0] ?></td></tr>
                        <tr class="info"><td>Birth Date</td><td><?php echo $birth_date[0] ?></td></tr>
                        <tr class="info"><td>Age</td><td><?php echo $age[0] ?></td></tr>
                        <tr class="info"><td>Nationality</td><td><?php echo $nationality[0] ?></td></tr>
                        <tr class="info"><td>Religion</td><td><?php echo $religion[0] ?></td></tr>
                        <tr class="info"><td>Occupation</td><td><?php echo $occupation[0] ?></td></tr>
                        <tr class="info"><td>Education</td><td><?php echo $education[0] ?></td></tr>
                        <tr class="info"><td>Phone No</td><td><?php echo $phone_no[0] ?></td></tr>
                        <tr class="info"><td>Status</td><td><?php echo $status[0] ?></td></tr>
                        <tr class="info"><td>Comment</td><td><?php echo $comment[0] ?></td></tr>



                        <!--array('dr', 'cr', 'date'trns_comment)-->



                    </table>
                </div>
            </div>
        </div>

    </div>

    <div class="col-lg-8">
        <div class="panel panel-teal">
            <div class="panel-heading">
                Transaction Information
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <?php
                        if ($account_type == "Loan") {
                            echo"<strong class='pull-left' style='border-bottom:1px solid #ccc'>Remaining Interest Amount: $interest_amount[0]</strong>
                        <strong class='pull-right'><button id='check_all_interest_amount' class='btn btn-info' style='width:100px' account_no='$account_number[0]' data-toggle='modal' data-target='#interest_modal'>All Interest</button></strong><br><br>";
                        }
                        ?>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-inline" >
                            <div class="form-group">
                                <label>Date1</label>
                                <input type="date" class="form-control" id="date1">
                            </div>
                            <div class="form-group">
                                <label>Date2</label>
                                <input type="date" class="form-control" id="date2">
                            </div>

                            <button type="submit" class="btn btn-default" id="filter">Filter</button>
                            <button style="width:100px" content="#print_trans_history" class="btn btn-info btn-print_this_page pull-right" href="">print</button>
                        </div>
                        <br>
                    </div>

                    <div class="col-lg-12" id="print_trans_history">
                        <div class="panel panel-primary" style="padding: 3px">
                            <div class="panel-heading">
                                <strong>Transaction History of <?php echo $name[0] ?><br>
                                    Member No:<?php echo $member_no[0] ?>
                                    <br>
                                    Account Number:<?php echo $account_number[0] ?>
                                </strong>
                            </div>
                            
                            <input id="acc_info" type="hidden" value="<?php echo $account_type . "/" . $account_number[0] ?>"/>
                            <div class="panel-body" id="trans_history">
                                <?php
                                $this->load->view('vadmin/common_page/v_filter_customer_transaction');
                                ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>
</div>

<div class="modal fade" id="interest_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2> All Taken Interest Amount From <?php echo $name[0] ?></h2>
            </div>
            <div class="modal-body" id="interest_modal_value">

            </div>   
             <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $("#filter").click(function(){
            date1=$("#date1").val();
            date2=$("#date2").val();
            acc_info=$("#acc_info").val();
            page="../transaction/filter_customer_transaction/"+acc_info+"/"+date1+"/"+date2
            change_content("#trans_history",page);
        });
        $("#check_all_interest_amount").click(function(){
            account_no=$(this).attr("account_no");
            page="../account/get_all_loan_inerest_value/"+account_no;
            change_content("#interest_modal_value",page);
        })
    })
</script>