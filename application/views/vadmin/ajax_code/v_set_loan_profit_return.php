
    <div class="row">
        <div class="col-lg-8 col-md-offset-2">
            <div class="panel panel-teal">
                <div class="panel-heading">
                    Set Loan Interest Amount
                </div>
                <div class="panel-body">
                    <form style="border: none" content="alert_box1" class='change_content_by_alert' action='../upload/upload_loan_profit' method='POST' enctype='multipart/form-data'>
                        <div class="col-lg-12">
                            <div class="form-group">
                                Select Account Number
                                <select class="form-control" name="loan_account_no">
                                    <?php
                                    if (isset($acc_no)) {
                                        foreach ($acc_no as $option) {
                                            echo"<option>$option</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                Start Date
                                <input class="form-control" type="date" name="start_date"/>
                            </div>
                            <div class="form-group">
                                End Date
                                <input class="form-control" type="date" name="end_date"/>
                            </div>
                            <div class="form-group">
                                Amount
                                <input class="form-control" name="amount"/>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info" data-toggle="modal" data-target="#alert_modal">Submit</button>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>

    </div>
<?php
                    include_once 'alert_modal.php';
                    ?>