 <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="../plugins/morris/morris.min.js"></script>
<div class="row">
    <div class="col-md-6">
        <!-- AREA CHART -->
        <div class="panel panel-teal">
            <div class="panel-heading">
                Savings A/C Chart
            </div>
            <div class="panel panel-body">
                <div class="chart" id="saving_account_chart" style="min-height: 290px;"></div>
            </div>
        </div>

    </div>
    <div class="col-md-6">
        <!-- AREA CHART -->
        <div class="panel panel-teal">
            <div class="panel-heading">
               Loan A/C Chart
            </div>
            <div class="panel panel-body">
                <div class="chart" id="loan_account_chart" style="min-height: 290px;"></div>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <!-- AREA CHART -->
        <div class="panel panel-teal">
            <div class="panel-heading">
               All A/C Chart
            </div>
            <div class="panel panel-body">
                <div class="chart" id="all_account_chart" style="min-height: 290px;"></div>
            </div>
        </div>

    </div>
    <div class="col-md-6">
        <!-- AREA CHART -->
        <div class="panel panel-teal">
            <div class="panel-heading">
               Account Number
            </div>
            <div class="panel panel-body">
                 <div class="chart" id="accountnumber-chart" style="min-height: 290px;"></div>
            </div>
        </div>

    </div>

</div><!-- /.row -->

<script>
    $(function () {
        "use strict";

        //DONUT CHART
        var donut = new Morris.Donut({
            element: 'accountnumber-chart',
            resize: true,
            colors: ["#3c8dbc", "#f56954", "#00a65a"],
            data:<?php print_r(json_encode($morris_data1)) ?>,
            hideHover: 'auto'
        });
        //BAR CHART
        var bar = new Morris.Bar({
            element: 'saving_account_chart',
            resize: true,
            data:<?php print_r(json_encode($morris_data3)) ?>,
            barColors: ['#00a65a', '#f56954'],
            xkey: 'y',
            ykeys: ['a', 'b'],
            labels: ['Debit', 'Credit'],
            hideHover: 'auto'
        });
        var bar = new Morris.Bar({
            element: 'loan_account_chart',
            resize: true,
            data:<?php print_r(json_encode($morris_data4)) ?>,
            barColors: ['#00a65a', '#f56954'],
            xkey: 'y',
            ykeys: ['a', 'b'],
            labels: ['Debit', 'Credit'],
            hideHover: 'auto'
        });
        var bar = new Morris.Bar({
            element: 'all_account_chart',
            resize: true,
            data:<?php print_r(json_encode($morris_data2)) ?>,
            barColors: ['#00a65a', '#f56954'],
            xkey: 'y',
            ykeys: ['a', 'b'],
            labels: ['Debit', 'Credit'],
            hideHover: 'auto'
        });
    });
</script>
