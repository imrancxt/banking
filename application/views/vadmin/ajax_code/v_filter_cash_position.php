<div class="col-lg-12">
    <div class="table-responsive">
        <table class="table table-bordered table-hover" id="table1">
                <tr>
                    <th>
                        Opening Balance(+)
                    </th>
                    <th>
                        <?php echo $opening_amount ?>
                    </th>
                </tr>
                <tr>
                    <th>
                        Received From Bank/Branch(+)
                    </th>
                    <th>
                        
                    </th>
                </tr>
                <tr>
                    <th>
                        Received From Client(+)
                    </th>
                    <th>
                        <?php
                        echo $received_from_client[0];
                        ?>
                    </th>
                </tr>
                <tr>
                    <th>
                        <strong>Total Cash Received</strong>
                    </th>
                    <th>
                        <?php
                        $total_cash_received=$received_from_client[0]+$opening_amount;
                        echo $total_cash_received;
                        ?>
                    </th>
                </tr>
                <tr>
                    <th>
                        Paid To Bank/Branch(-)
                    </th>
                    <th></th>
                </tr>
                <tr>
                    <th>
                        Paid To Client(-)
                    </th>
                     <th>
                        <?php
                        echo $paid_to_client[0];
                        ?>
                    </th>
                </tr>
                <tr>
                    <th>
                        <strong>Total Cash In Hand</strong>
                    </th>
                    <th>
                     <?php
                        $total_cash_in_hand=$total_cash_received-$paid_to_client[0];
                        echo $total_cash_in_hand;
                        ?>
                    </th>
                </tr>
        </table>
    </div>
</div>
