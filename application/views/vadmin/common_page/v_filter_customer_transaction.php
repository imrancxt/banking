<strong>Transaction Date:<?php echo $trans_date ?>
</strong>
<?php

if (isset($nett_dr)) {
    $this->load->view('vadmin/common_page/nett_ledger.php');
}
if (isset($interest_dr)) {
    $this->load->view('vadmin/common_page/interest_ledger.php');
}

if (isset($service_dr)) {
    $this->load->view('vadmin/common_page/service_ledger.php');
}

if (isset($book_dr)) {
    $this->load->view('vadmin/common_page/book_ledger.php');
}
?>