<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of api
 *
 * @author imran
 */
class api extends CI_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
        $this->load->library('view_page');
        $this->admin = 1;
        $this->load->model('mapi');
    }

    function index() {
        $this->view_page->admin_page('home', "");
    }

    //API

    public function categorylist() {
        $query = "select t1.serial as category_id,t1.category_name,(select count(t2.serial) from video_sub_category as t2 where t2.category_id=t1.serial) as sub_category_number,
               (select count(t3.serial) from video_list as t3 where t3.category_id=t1.serial) as video_number from video_category as t1 group by t1.serial";
        $data['status'] = "1";
        $data['data1'] = $this->mapi->get_category_list($query);
        $query2 = "SELECT t1.category_id,t1.sub_category_id,t2.sub_category_name,t1.chapter_no,(select count(t2.serial) from video_list as t2 where t1.sub_category_id=t2.sub_category_id) as video_number FROM 
            `video_list` as t1,video_sub_category as t2 where t1.sub_category_id=t2.serial group by t1.sub_category_id";
        $data['data2'] = $this->mapi->get_category_list2($query2);
        $info = json_encode($data);
        print_r($info);
    }

    public function subcategorylist() {
        $category_id = $_GET['category_id'];
        $query = "SELECT t1.serial as sub_category_id,t1.sub_category_name,(select count(t2.serial) from video_list as t2 
        where t2.category_id=$category_id and t2.sub_category_id=t1.serial)as video_number FROM `video_sub_category` as t1 
        where t1.category_id=$category_id group by t1.serial";
        $data['status'] = "1";
        $data['data'] = $this->mapi->get_sub_category_list($query);
        $info = json_encode($data);
        print_r($info);
    }

    public function signupmail() {
        $username = $_POST['username'];
        $password = sha1($_POST['password']);
        $usermail = $_POST['usermail'];
        $data = array('username' => $username, 'usermail' => $usermail, 'password' => $password, 'usertype' => 'normal');
        if ($this->db->insert('users', $data)) {
            $query = "select * from users where usermail='$usermail' and password='$password'";
            $data = $this->mapi->get_user($query);
            $info = json_encode($data);
            print_r($info);
        }
    }

    public function signout() {
        $data['status'] = "1";
        $json = json_encode($data);
        print_r($json);
    }

    public function signupfb() {
        $username = $_POST['username'];
        $usermail = $_POST['usermail'];
        $data = array('username' => $username, 'usermail' => $usermail, 'password' => "", 'usertype' => 'fb');
        if ($this->db->insert('users', $data)) {
            $query = "select * from users where usermail='$usermail'";
            $data = $this->mapi->get_user($query);
            $info = json_encode($data);
            print_r($info);
        }
    }

    public function loginmail() {
        $usermail = $_POST['usermail'];
        $password = sha1($_POST['password']);
        /* $usermail='imran@yahoo.com';
          $password='123'; */
        $query = "select * from users where usermail='$usermail' and password='$password'";
        $data = $this->mapi->get_user($query);
        $info = json_encode($data);
        print_r($info);
    }

    public function loginfb() {
        $usermail = $_POST['usermail'];
        $query = "select * from users where usermail='$usermail'";
        $data = $this->mapi->get_user($query);
        $info = json_encode($data);
        print_r($info);
    }

    public function likevideo() {
        $userid = $_GET['userid'];
        $video_id = $_GET['video_id'];
        $query1 = "select * from users where serial=$userid";
        $data1 = $this->mapi->get_user($query1);
        if (isset($data1['userid'])) {
            $query2 = "SELECT * FROM `like_video` where userid=$userid and video_id=$video_id and status='liked'";
            $rs2 = $this->db->query($query2);
            if ($rs2->num_rows() > 0) {
                if ($this->db->query("update like_video set status='like' where userid=$userid and video_id=$video_id")) {
                    $query3 = "select * from like_video where userid=$userid and video_id=$video_id";
                    $data3 = $this->mapi->get_like_id($query3);
                    $info = json_encode($data3);
                    print_r($info);
                }
            } else {
                $query3 = "SELECT * FROM `like_video` where userid=$userid and video_id=$video_id and status='like'";
                $rs3 = $this->db->query($query3);
                if ($rs3->num_rows() > 0) {
                    $this->db->query("update like_video set status='liked' where userid=$userid and video_id=$video_id");
                    $query4 = "select * from like_video where userid=$userid and video_id=$video_id";
                    $data4 = $this->mapi->get_like_id($query4);
                    $info = json_encode($data4);
                    print_r($info);
                } else {
                    if ($this->db->insert('like_video', array('userid' => $userid, 'video_id' => $video_id, 'status' => 'liked'))) {
                        $query4 = "select * from like_video where userid=$userid and video_id=$video_id";
                        $data4 = $this->mapi->get_like_id($query4);
                        $info = json_encode($data4);
                        print_r($info);
                    }
                }
            }
        }
    }

    public function unlikevideo() {
        $userid = $_GET['userid'];
        $likeid = $_GET['likeid'];
        $query1 = "select * from users where serial=$userid";
        $data1 = $this->mapi->get_user($query1);
        if (isset($data1['userid'])) {
            $query3 = "update like_video set status='like' where userid=$userid and serial=$likeid";
            $this->db->query($query3);
            $data['status'] = "1";
            $info = json_encode($data);
            print_r($info);
        }
    }

    public function commentvideo() {//have to use post method
        $userid = $_GET['userid'];
        $video_id = $_GET['video_id'];
        $comment = $_GET['comment'];
        // echo $comment;
        $data = array('userid' => $userid, 'video_id' => $video_id, 'comment' => $comment);
        $query1 = "select * from users where serial=$userid";
        $data1 = $this->mapi->get_user($query1);
        if (isset($data1['userid'])) {
            if ($this->db->insert('comment_video', $data)) {
                $query2 = "SELECT * FROM `comment_video` where userid=$userid and video_id=$video_id order by serial desc limit 0,1";
                $data2 = $this->mapi->get_comment_id($query2);
                $info = json_encode($data2);
                print_r($info);
            }
        }
    }

    public function deletecommentvideo() {
        $comment_id = $_GET['commentid'];
        $query = "delete  from comment_video where serial=$comment_id";
        if ($this->db->query($query)) {
            $data['status'] = "1";
            $info = json_encode($data);
            print_r($info);
        }
    }

    public function subcategoryvideolist() {
        $category_id = $_GET['category_id'];
        $subcategory_id = $_GET['subcategory_id'];
        $query = "SELECT t1.published_date,t1.teacher_name,t1.chapter_no,t1.topic_name, t1.serial as video_id,t1.video_link as video_link,t1.name as video_name,t1.about as video_about,t1.category_id,t1.sub_category_id,(select count(t2.video_id) from like_video as t2 
                where t1.serial=t2.video_id and t2.status='liked') as like_number,(select count(t3.video_id) from comment_video as t3 where t1.serial=t3.video_id) as comment_number,
               (select count(t4.serial) from view_video as t4 where t1.serial=t4.video_id) as view_number
                FROM `video_list` as t1
                where t1.category_id=$category_id and t1.sub_category_id=$subcategory_id";
        $data['status'] = "1";
        $data['data'] = $this->mapi->get_subcategoryvideolist($query);
        $json = json_encode($data);
        print_r($json);
    }

    public function videoinfo() {
        $video_id = $_GET['video_id'];
        $userid = $_GET['userid'];
        $this->db->insert('view_video', array('video_id' => $video_id, 'userid' => $userid));
        $query = "SELECT t1.serial, t1.video_link as video_link,t1.category_id,t1.sub_category_id,t1.name as video_name,t1.about,(select count(t2.video_id) from like_video as t2  where t1.serial=t2.video_id and t2.status='liked') as like_number,
                (select count(t3.video_id) from comment_video as t3 where t1.serial=t3.video_id) as comment_number,t1.teacher_name,t1.chapter_no,t1.topic_name,
                (select count(t4.serial) from view_video as t4 where t1.serial=t4.video_id) as view_number
                 FROM `video_list` as t1 where t1.serial=$video_id";
        $data['status'] = "1";
        $data['data'] = $this->mapi->get_videoinfo($query);
        $query2 = "SELECT t1.serial as comment_id,t1.date,t1.comment,t2.serial,t2.username FROM `comment_video` as t1,users as t2
                 where t2.serial=t1.userid and t1.video_id=$video_id";
        $data['data2'] = $this->mapi->get_videocomment($query2);
        $query3 = "SELECT * FROM `like_video` where userid=$userid and video_id=$video_id and status='liked'";
        $data['like'] = $this->mapi->check_like($query3);

        $query4 = "SELECT * FROM `bookmark` where userid=$userid and video_id=$video_id";
        $data['bookmark'] = $this->mapi->check_like($query4); //actually bookmark

        $json = json_encode($data);
        print_r($json);
    }

    public function watchedvideolist() {
        $userid = $_GET['userid'];
        $query = "SELECT  distinct(t1.serial) as video_id,  t1.published_date,t1.video_link as video_link,t1.name as video_name,t1.about as video_about,t1.category_id,t1.sub_category_id,
                (select count(t2.video_id) from like_video as t2 where t1.serial=t2.video_id and t2.status='liked') as like_number,
                (select count(t3.video_id) from comment_video as t3 where t1.serial=t3.video_id) as comment_number,
                
                (select count(t5.serial) from view_video as t5 where t1.serial=t5.video_id) as view_number,
                t1.teacher_name,t1.chapter_no,t1.topic_name
                 FROM `video_list` as t1,`view_video` as t4 where t4.userid=$userid and t1.serial=t4.video_id group by t1.serial order by t1.serial desc limit 0,8";
        $data['status'] = "1";
        $data['data'] = $this->mapi->get_subcategoryvideolist($query);
        $json = json_encode($data);
        print_r($json);
    }

    public function popularvideolist() {
        $query = "SELECT t1.published_date, t1.serial as video_id,t1.video_link as video_link,t1.name as video_name,t1.about as video_about,t1.category_id,t1.sub_category_id,
                (select count(t2.video_id) from like_video as t2 where t1.serial=t2.video_id and t2.status='liked') as like_number,
                (select count(t3.video_id) from comment_video as t3 where t1.serial=t3.video_id) as comment_number,t1.teacher_name,t1.chapter_no,t1.topic_name,
                (select count(t4.serial) from view_video as t4 where t1.serial=t4.video_id) as view_number
                 FROM `video_list` as t1 order by  view_number desc limit 0,24";
        $data['status'] = "1";
        $data['data'] = $this->mapi->get_subcategoryvideolist($query);
        $json = json_encode($data);
        print_r($json);
    }

    public function newvideolist() {
        $query = "SELECT t1.published_date, t1.serial as video_id,t1.video_link as video_link,t1.name as video_name,t1.about as video_about,t1.category_id,t1.sub_category_id,
                (select count(t2.video_id) from like_video as t2 where t1.serial=t2.video_id and t2.status='liked') as like_number,
                (select count(t3.video_id) from comment_video as t3 where t1.serial=t3.video_id) as comment_number,t1.teacher_name,t1.chapter_no,t1.topic_name,
                (select count(t4.serial) from view_video as t4 where t1.serial=t4.video_id) as view_number
                 FROM `video_list` as t1 order by rand() limit 0,24";
        $data['status'] = "1";
        $data['data'] = $this->mapi->get_subcategoryvideolist($query);
        $json = json_encode($data);
        print_r($json);
    }

    public function imagevideo() {
        $videoidlist = $_GET['videoidlist'];
        $this->db->join('video_list t2', 't1.serial = t2.serial');
        $this->db->where_in('t1.serial', $videoidlist);
        $rs = $this->db->get('video_list t1');
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info[$i] = array('image' => $row->image);
                $i++;
            }
            $data['status'] = "1";
            $data['data'] = $info;
            $json = json_encode($data);
            print_r($json);
        }
    }

    public function imageuser() {
        $useridlist = $_GET['useridlist'];
        $this->db->join('users t2', 't1.serial = t2.serial');
        $this->db->where_in('t1.serial', $useridlist);
        $rs = $this->db->get('users t1');
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                $info[$i] = array('image' => $row->image);
                $i++;
            }
            $data['status'] = "1";
            $data['data'] = $info;
            $json = json_encode($data);
            print_r($json);
        }
    }

    public function chapterlist() {
        $category_id = $_GET['category_id'];
        $sub_category_id = $_GET['sub_category_id'];
        $query = "SELECT t1.chapter_no,t1.chapter_name,t1.topic_name,(select count(t2.serial) from video_list as t2 where t2.category_id=$category_id and t2.sub_category_id=$sub_category_id and  t2.chapter_no=t1.chapter_no) as video_no FROM 
              `video_list` as t1 WHERE t1.category_id=$category_id and t1.sub_category_id=$sub_category_id group by t1.chapter_no";
        $data['status'] = "1";
        $data['data'] = $this->mapi->getchapterlist($query);
        $json = json_encode($data);
        print_r($json);
    }

    public function topiclist() {
        $category_id = $_GET['category_id'];
        $sub_category_id = $_GET['sub_category_id'];
        $chapter_no = $_GET['chapter_no'];
        $query = "SELECT t1.topic_name,(select count(t2.serial) from video_list as t2 where t2.category_id=$category_id and t2.sub_category_id= $sub_category_id and t2.chapter_no='$chapter_no' and  t2.chapter_no=t1.chapter_no) as video_no FROM `video_list` as t1 
               WHERE t1.category_id= $category_id and t1.sub_category_id=$sub_category_id and t1.chapter_no='$chapter_no' group by t1.category_id,t1.sub_category_id";

        $data['status'] = "1";
        $data['data1'] = $this->mapi->gettopiclist($query);
        $query2 = "SELECT t1.teacher_name,t1.chapter_no,t1.published_date,t1.topic_name, t1.serial as video_id,t1.video_link as video_link,t1.name as video_name,t1.about as video_about,t1.category_id,t1.sub_category_id,(select count(t2.video_id) from like_video as t2 
                where t1.serial=t2.video_id and t2.status='liked') as like_number,(select count(t3.video_id) from comment_video as t3 where t1.serial=t3.video_id) as comment_number,
                (select count(t4.serial) from view_video as t4 where t1.serial=t4.video_id) as view_number
                FROM `video_list` as t1
                where t1.category_id=$category_id and t1.sub_category_id=$sub_category_id and chapter_no='$chapter_no'";
        $data['data2'] = $this->mapi->get_subcategoryvideolist($query2);

        $json = json_encode($data);
        print_r($json);
    }

    public function topicvideolist() {
        /* $category_id = $_GET['category_id'];
          $sub_category_id = $_GET['sub_category_id'];
          $chapter_no = $_GET['chapter_no'];
          $topic_name=$_GET['topic_name'];
          $query = "SELECT t1.serial, t1.video_link as video_link,t1.category_id,t1.sub_category_id,t1.name as video_name,t1.about,(select count(t2.video_id) from like_video as t2  where t1.serial=t2.video_id and t2.status='like') as like_number,
          (select count(t3.video_id) from comment_video as t3 where t1.serial=t3.video_id) as comment_number,t1.teacher_name,t1.chapter_no,t1.topic_name FROM `video_list` as t1 where t1.serial=$video_id";
          $data['status'] = "1";
          $data['data'] = $this->mapi->get_videoinfo($query);
          $query2 = "SELECT t1.serial as comment_id,t1.date,t1.comment,t2.serial,t2.username FROM `comment_video` as t1,users as t2
          where t2.serial=t1.userid and t1.video_id=$video_id";
          $data['data2'] = $this->mapi->get_videocomment($query2);
          $query3 = "SELECT * FROM `like_video` where userid=$userid and video_id=$video_id and status='unlike'";
          $data['like'] = $this->mapi->check_like($query3);
          $json = json_encode($data);
          print_r($json); */
    }

    private function get_data($query, $var, $col) {
        $info = array();
        $rs = $this->db->query($query);
        if ($rs->num_rows() > 0) {
            $i = 0;
            foreach ($rs->result() as $row) {
                for ($j = 0; $j < count($var); $j++) {
                    $info[$var[$j]][$i] = $row->$col[$j];
                }
                $i++;
            }
        }
        return $info;
    }

    public function chapterlistall() {
        $query = "SELECT t1.category_id,t1.sub_category_id,t1.chapter_no,t1.chapter_name,t1.topic_name,(select count(serial) from video_list as t2 where t1.category_id=t2.category_id and t1.sub_category_id=t2.sub_category_id and t1.chapter_no=t2.chapter_no) as video_number 
               FROM `video_list` as t1 group by t1.chapter_no, t1.category_id,t1.sub_category_id";
        $data = $this->mapi->get_chapterlistall($query);
        print_r($data);
    }

    public function search() {
        $info = $_GET['info'];
        $query = "SELECT t1.published_date, t1.serial as video_id,t1.video_link as video_link,t1.name as video_name,t1.about as video_about,t1.category_id,t1.sub_category_id,
                (select count(t2.video_id) from like_video as t2 where t1.serial=t2.video_id and t2.status='liked') as like_number,
                (select count(t3.video_id) from comment_video as t3 where t1.serial=t3.video_id) as comment_number,t1.teacher_name,t1.chapter_no,t1.topic_name,
                (select count(t4.serial) from view_video as t4 where t1.serial=t4.video_id) as view_number
                 FROM `video_list` as t1  where t1.name like '%$info%'";
        $data['status'] = "1";
        $data['data'] = $this->mapi->get_subcategoryvideolist($query);
        $json = json_encode($data);
        print_r($json);
        //echo $query;
    }

    public function newvideoinfo() {
        
    }

    public function send_mail() {
        $email = $_GET['useremail'];
        $query = "select * from users where usermail='$email' and usertype='normal'";
        $rs = $this->db->query($query);
        $data['status'] = "1";
        //echo $query;
        if ($rs->num_rows() > 0) {
            $new_password = $this->get_rand_string();
            $hash_password=sha1($new_password);
            $query = "update `users` set password='$hash_password' where usermail='$email'";
            $this->db->query($query);
            $to = "$email";
            $subject = "Onnorokompathshala password";
            $message = "Your New Password Is:$new_password";
            $header = "From:techcarebd.com \r\n";
            $retval = mail($to, $subject, $message, $header);
            if ($retval == true) {
                $data['data'] = "1";
            } else {
                $data['data'] = "0";
            }
        } else {
            $data['data'] = "0";
        }
        $json = json_encode($data);
        print_r($json);
    }

    private function get_rand_string() {
        $length = 6;
        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
        return $randomString;
    }

    public function notification() {
        $push_date = $_GET['date'];
        $query = "SELECT * FROM `video_list` where push_date>='$push_date'";
        $rs = $this->db->query($query);
        $data = array();
        if ($rs->num_rows() > 0) {
            $data['status'] = "1";
        } else {
            $data['status'] = "0";
        }
        $json = json_encode($data);
        print_r($json);
    }

    public function createfolder() {
        $userid = $_GET['userid'];
        $folder_name = $_GET['foldername'];
        if ($folder_name != "") {
            $cq = "select serial from `folder_name` where folder_name='$folder_name' and userid=$userid";
            $rs = $this->db->query($cq);
            if ($rs->num_rows() == 0) {
                $info = array('userid' => $userid, 'folder_name' => $folder_name);
                if ($this->db->insert('folder_name', $info)) {
                    $query = "select serial from folder_name where userid=$userid and folder_name='$folder_name' order by serial desc limit 1";
                    $data = $this->get_data($query, array('folder_id'), array('serial'));
                    $info1['status'] = 1;
                    $info1['folder_id'] = $data['folder_id'][0];
                    $json = json_encode($info1);
                    print_r($json);
                }
            }
        }
    }

    public function removefolder() {
        $userid = $_GET['userid'];
        $folderid = $_GET['folderid'];
        if ($this->db->delete('folder_name', array('userid' => $userid, 'serial' => $folderid)) && $this->db->delete('bookmark_videolist', array('userid' => $userid, 'folder_id' => $folderid))) {
            $data['status'] = 1;
            $json = json_encode($data);
            print_r($json);
        }
    }

    public function set_video_to_mainbookmark() {
        $userid = $_GET['userid'];
        $video_id = $_GET['video_id'];
        $data = array('userid' => $userid, 'video_id' => $video_id);
        if ($this->db->insert('bookmark', $data)) {
            $info['status'] = 1;
            $json = json_encode($info);
            print_r($json);
        }
    }

    public function remove_video_from_mainbookmark() {
        $userid = $_GET['userid'];
        $video_id = $_GET['video_id'];
        if ($this->db->delete('bookmark', array('userid' => $userid, 'video_id' => $video_id))) {
            $data['status'] = 1;
            $json = json_encode($data);
            print_r($json);
        }
    }

    public function add_video_to_folder() {
        $userid = $_GET['userid'];
        $video_id = $_GET['video_id'];
        $folder_id = $_GET['folder_id'];
        $data = array('userid' => $userid, 'folder_id' => $folder_id, 'video_id' => $video_id);
        if ($this->db->insert('bookmark_videolist', $data) && $this->db->delete('bookmark', array('userid' => $userid, 'video_id' => $video_id))) {
            $info['status'] = 1;
            $json = json_encode($info);
            print_r($json);
        }
    }

    public function remove_video_from_folder() {
        $userid = $_GET['userid'];
        $video_id = $_GET['video_id'];
        $folder_id = $_GET['folder_id'];
        if ($this->db->delete('bookmark_videolist', array('userid' => $userid, 'folder_id' => $folder_id, 'video_id' => $video_id))) {
            $data['status'] = 1;
            $json = json_encode($data);
            print_r($json);
        }
    }

    public function getvideo_from_mainbookmark() {
        $userid = $_GET['userid'];
        $query = "SELECT  distinct(t1.serial) as video_id,  t1.published_date,t1.video_link as video_link,t1.name as video_name,t1.about as video_about,t1.category_id,t1.sub_category_id,
                (select count(t2.video_id) from like_video as t2 where t1.serial=t2.video_id and t2.status='liked') as like_number,
                (select count(t3.video_id) from comment_video as t3 where t1.serial=t3.video_id) as comment_number,
                
                (select count(t5.serial) from view_video as t5 where t1.serial=t5.video_id) as view_number,
                t1.teacher_name,t1.chapter_no,t1.topic_name
                 FROM `video_list` as t1,`bookmark` as t4 where t4.userid=$userid and t1.serial=t4.video_id";
        $data['status'] = "1";
        $data['data'] = $this->mapi->get_subcategoryvideolist($query);
        $json = json_encode($data);
        print_r($json);
    }

    public function get_video_from_folder() {
        $userid = $_GET['userid'];
        $folderid = $_GET['folderid'];
        $query = "SELECT  distinct(t1.serial) as video_id,  t1.published_date,t1.video_link as video_link,t1.name as video_name,t1.about as video_about,t1.category_id,t1.sub_category_id,
                (select count(t2.video_id) from like_video as t2 where t1.serial=t2.video_id and t2.status='liked') as like_number,
                (select count(t3.video_id) from comment_video as t3 where t1.serial=t3.video_id) as comment_number,
                
                (select count(t5.serial) from view_video as t5 where t1.serial=t5.video_id) as view_number,
                 t1.teacher_name,t1.chapter_no,t1.topic_name
                 FROM `video_list` as t1,`bookmark_videolist` as t4 where t4.userid=$userid and t1.serial=t4.video_id and t4.folder_id=$folderid";
        $data['status'] = "1";
        $data['data'] = $this->mapi->get_subcategoryvideolist($query);
        $json = json_encode($data);
        print_r($json);
    }

    public function folderlist() {
        $userid = $_GET['userid'];
        $query = "SELECT t1.serial as folder_id,t1.folder_name,(select count(t2.serial) from bookmark_videolist as t2 
                    where t1.serial=t2.folder_id and t1.userid=t2.userid) as total_video 
                    FROM `folder_name` as t1 where t1.userid=$userid";
        $data['status'] = "1";
        $data['data'] = $this->mapi->get_folderlist($query);
        $json = json_encode($data);
        print_r($json);
    }
    public function test_user(){
        $user="mbrony57@gmail.com";
        $query="select * from users where usermail='$user'";
        $rs=$this->db->query($query);
        foreach($rs->result() as $row){
            echo $row->usermail.",".$row->usertype;
        }
    }

}

?>
