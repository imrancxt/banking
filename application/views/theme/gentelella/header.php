<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Admin Panel</title>

        <!-- Bootstrap -->
        <link href="../theme-gentelella/css/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../theme-gentelella/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../theme-gentelella/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="../theme-gentelella/css/custom.css" rel="stylesheet">

        <!-- jQuery -->
        <script src="../theme-gentelella/js/jquery/dist/jquery.min.js"></script>
          <script src="../dist/js/customize.js"></script>
        <script src="../dist/js/print.js"></script>
    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Somobai Management Software</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                  
                   
                    <!-- /.dropdown -->
                    <!-- <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                              Change Theme
                                        <span class=" fa fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                           <li>
                                <a href="#" class="change_theme" theme="xtheme">
                                    <i class="fa fa-fw fa-weibo"></i><span data-localizem="theme_avenger">Xtheme</span></a>
                            </li>
          
                             <li>
                                <a href="#" class="change_theme" theme="gentelella">
                                    <i class="fa fa-fw fa-circle-o"></i><span data-localize="theme_teselella">Gentelella</span></a>
                            </li>
                        </ul>
                        
                    </li>-->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                              Back-up Database
                                        <span class=" fa fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                           <li>
                            <a href='http://localhost/phpmyadmin/#PMAURL-2:db_export.php?db=banking&table=&server=1&target=&token=4b812e06a5eec956e69985dea8203ea1' target="_blank"> <i class="fa fa-exchange"></i><span>Export</span></a>
                        </li>

                        <li>
                            <a href='http://localhost/phpmyadmin/#PMAURL-3:db_import.php?db=banking&table=&server=1&target=&token=4b812e06a5eec956e69985dea8203ea1' target="_blank"> <i class="fa fa-venus-mars"></i><span>Import</span></a>
                        </li>
                        </ul>
                        <!-- /.dropdown-alerts -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li><a href="../panel/profile">Profile</a></li>
                                        <li><a href="../panel/user_management"><span data-localize="nav_user_management">User Management</span></a></li>
                                       
                                        <li><a href="../panel/signout"><span>Sign Out</span> <i class="pull-right fa fa-sign-out"></i></a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input id="search" type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                <!-- /input-group -->
                            </li>

                           
                            <li><a href="../panel/"><i class="fa fa-home"></i> Dashboard</a></li>
                            
                             <li><a href="#"><i class="fa fa-edit"></i> Member<span class="fa arrow"></span></a>
                                            <ul class="nav nav-second-level">
                                                <li><a href="../member/current_member">Current Member</a></li>
                                                <li><a  href="../member/previous_member">Previous Member</a></li>
                                                <?php
                                                if ($_SESSION['user_type'] != "sales") {
                                                    echo"<li><a  href='../member/add_member'>Add Member</a></li>";
                                                }
                                                ?>

                                            </ul>
                                        </li>

                            <li><a href='#'><i class='fa fa-desktop'></i> Open Account<span class="fa arrow"></span></a>
                                            <ul class="nav nav-second-level">
                                                <li><a  href='../member/open_savings_account'>Open Savings Account</a></li>
                                                <li><a  href='../member/open_loan_account'>Open Loan Account</a></li>
                                            </ul>
                                        </li>
                                        
                               <li><a href="#"><i class="fa fa-table"></i> Account Head/Subhead <span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                                    <a  href='../account/'>
                                                        <span>Account Head</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a  href='../account/account_subhead'>
                                                        <span>Account Sub Head</span>
                                                    </a>
                                                </li>
                                </ul>
                            </li>          
                            <li><a href="#"><i class="fa fa-desktop"></i> Supplementary <span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">

                                    <li><a  href="../transaction/open_supplementary">Add Supplementary1</a></li>
                                                <li><a  href="../transaction/open_supplementary2">Add Supplementary2</a></li>
                                                <li><a  href="../account/view_supplementary1">View Supplementary1</a></li>
                                                <li><a  href="../account/view_supplementary2">View Supplementary2</a></li>

                                </ul>
                            </li>
                            
                            
                          

                           
                            
                            <li><a href="#"><i class="fa fa-bug"></i> Accounts <span class="fa arrow"></span></a>
                                        <ul class="nav nav-second-level">
                                           <li><a  href="../member/all_savings_account">Savings Account</a></li>
                                                <li><a  href="../member/all_loan_account">Loan Account</a></li>
                                        </ul>
                                    </li>
                                    
                                 <li><a href="#"><i class="fa fa-windows"></i> General Ledger <span class="fa arrow"></span></a>
                                        <ul class="nav nav-second-level">
                                            <li><a  href="../transaction/get_general_ledger_data?account=S.B">S.B Account</a></li>
                                                <li><a  href="../transaction/get_general_ledger_data?account=Loan"> Loan Account</a></li>
                                                <li><a  href="../transaction/others_account">Other A/C Head</a></li>
                                                <li><a  href="../transaction/others_account_subhead">Other A/C Sub-Head</a></li>
                                                <li><a  href="../account/cash_in_hand_gl">Cash In Hand</a></li>
                                        </ul>
                                    </li>
                                    
                                    <li><a href="#"><i class="fa fa-sitemap"></i> Summary <span class="fa arrow"></span></a>
                                        <ul class="nav nav-second-level">
                                            <li><a  href="../transaction/get_ledger_summary?trans_type=dr">Debit</a></li>
                                                <li><a  href="../transaction/get_ledger_summary?trans_type=cr">Credit</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                            <a  href="../account/affair">
                                                <span class="icon"><i class="fa fa-envelope"></i></span> <span>Affair</span>
                                            </a>
                                        </li>
                                        <li><a  href="../account/set_loan_profit_return">
                                                <span class="icon"><i class="fa fa-bars"></i></span> <span>Set Interest</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a  href="../account/cash_position_memo">
                                                <span class="icon"><i class="fa fa-yen"></i></span> <span>Cash Position Memo</span>
                                            </a>
                                        </li>
                                         <li><a href="../login/signout"><span class="icon"><i class="ion-power"></i></span><span>Sign Out</span></a></li>
                            
                           
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <?php
                                if ($_SESSION['user_type'] == 'admin') {
                                    include_once 'bank_heading.php';
                                }
                                ?>