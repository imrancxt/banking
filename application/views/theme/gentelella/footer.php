</div>
<!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

</div>

<!-- Bootstrap -->
<script src="../theme-gentelella/js/bootstrap.min.js"></script>
<!-- FastClick -->


<!-- Custom Theme Scripts -->
<script src="../theme-gentelella/js/custom.js"></script>
<script src="../theme-gentelella/metisMenu/dist/metisMenu.min.js"></script>
<style>
    .btn,.panel{
        border-radius: 0px !important;
    }
</style>
<script>
    $(document).ready(function(){
        $(document).on('keyup','#search',function(){
            var $rows = $('#data_table .info');
            var orginal_row=0;
            var i=0;
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
            $rows.show().filter(function() {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                if(!~text.indexOf(val)==true){
                    i++;
                };
                orginal_row++;
                return !~text.indexOf(val);
            }).hide();
            //total_row=parseInt($rows)-i;
            $("#row_counter").html(orginal_row-i);
        });
        
        $(document).on("click",".btn-print_this_page",function(){
            content=$(this).attr("content");
            $(content).print();
        });
        $(".panel-teal").addClass("panel-default");
        
        $(".change_theme").click(function(){
            theme=$(this).attr("theme");
            page="../panel/theme/"+theme;
            
            window.localStorage.clear();
            page_refresh(page);
        })
    })
</script>

<style>
    .print_table {
        border-collapse: collapse;
        width:100%;
        border: 1px #ccc solid;
        margin-bottom: 20px;
    }
    .print_table td,.print_table th {
        border: 1px solid #ccc;
        height: 25px !important;
    }
    .print_table th{
        text-align: left !important;
    }
    .print_table tr{
        min-height: 50px !important;
    }
</style>
</body>
</html>