

                            </div> <!-- .container-fluid -->
                        </div> <!-- #page-content -->
                    </div>
                    <footer role="contentinfo">
                        <div class="clearfix">
                            <ul class="list-unstyled list-inline pull-left">
                                <li><h6 style="margin: 0;">&copy; 2016 Rahalait</h6></li>
                            </ul>
                            <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="fa fa-arrow-up"></i></button>
                        </div>
                    </footer>

                </div>
            </div>
        </div>

        <!-- Switcher -->
        <div class="demo-options">
            <div class="demo-options-icon"><i class="fa fa-tint"></i></div>
            <div class="demo-heading">Demo Settings</div>

            <div class="demo-body">
                <div class="tabular">
                    <div class="tabular-row">
                        <div class="tabular-cell">Fixed Header</div>
                        <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" checked data-size="mini" data-on-color="success" data-off-color="default" name="demo-fixedheader" data-on-text="&nbsp;" data-off-text="&nbsp;"></div>
                    </div>
                    <div class="tabular-row">
                        <div class="tabular-cell">Boxed Layout</div>
                        <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" data-size="mini" data-on-color="success" data-off-color="default" name="demo-boxedlayout" data-on-text="&nbsp;" data-off-text="&nbsp;"></div>
                    </div>
                    <div class="tabular-row">
                        <div class="tabular-cell">Collapse Leftbar</div>
                        <div class="tabular-cell demo-switches"><input class="bootstrap-switch" type="checkbox" data-size="mini" data-on-color="success" data-off-color="default" name="demo-collapseleftbar" data-on-text="&nbsp;" data-off-text="&nbsp;"></div>
                    </div>
                </div>
            </div>

            <div class="demo-body">
                <div class="option-title">Topnav</div>
                <ul id="demo-header-color" class="demo-color-list">
                    <li><span class="demo-cyan"></span></li>
                    <li><span class="demo-light-blue"></span></li>
                    <li><span class="demo-blue"></span></li>
                    <li><span class="demo-indigo"></span></li>
                    <li><span class="demo-deep-purple"></span></li> 
                    <li><span class="demo-purple"></span></li> 
                    <li><span class="demo-pink"></span></li> 
                    <li><span class="demo-red"></span></li>
                    <li><span class="demo-teal"></span></li>
                    <li><span class="demo-green"></span></li>
                    <li><span class="demo-light-green"></span></li>
                    <li><span class="demo-lime"></span></li>
                    <li><span class="demo-yellow"></span></li>
                    <li><span class="demo-amber"></span></li>
                    <li><span class="demo-orange"></span></li>               
                    <li><span class="demo-deep-orange"></span></li>

                    <li><span class="demo-bluegray"></span></li>


                    <li><span class="demo-gray"></span></li> 

                    <li><span class="demo-default"></span></li>
                    <li><span class="demo-bleachedcedar"></span></li>
                    <li><span class="demo-brown"></span></li>
                </ul>
            </div>

            <div class="demo-body">
                <div class="option-title">Sidebar</div>
                <ul id="demo-sidebar-color" class="demo-color-list">
                    <li><span class="demo-cyan"></span></li>
                    <li><span class="demo-light-blue"></span></li>
                    <li><span class="demo-blue"></span></li>
                    <li><span class="demo-indigo"></span></li>
                    <li><span class="demo-deep-purple"></span></li> 
                    <li><span class="demo-purple"></span></li> 
                    <li><span class="demo-pink"></span></li> 
                    <li><span class="demo-red"></span></li>
                    <li><span class="demo-teal"></span></li>
                    <li><span class="demo-green"></span></li>
                    <li><span class="demo-light-green"></span></li>
                    <li><span class="demo-lime"></span></li>
                    <li><span class="demo-yellow"></span></li>
                    <li><span class="demo-amber"></span></li>
                    <li><span class="demo-orange"></span></li>               
                    <li><span class="demo-deep-orange"></span></li>

                    <li><span class="demo-bluegray"></span></li>


                    <li><span class="demo-gray"></span></li> 
                    <li><span class="demo-bleachedcedar"></span></li>

                    <li><span class="demo-default"></span></li>
                    <li><span class="demo-brown"></span></li>
                </ul>
            </div>



        </div>
        <!-- /Switcher -->
        <!-- Load site level scripts -->

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->

      								<!-- Load Bootstrap -->
        <script src="../theme-xtheme/assets/js/enquire.min.js"></script> 									<!-- Load Enquire -->

        <script src="../theme-xtheme/assets/plugins/velocityjs/velocity.min.js"></script>					<!-- Load Velocity for Animated Content -->
        <script src="../theme-xtheme/assets/plugins/velocityjs/velocity.ui.min.js"></script>

        <script src="../theme-xtheme/assets/plugins/wijets/wijets.js"></script>     						<!-- Wijet -->

        <script src="../theme-xtheme/assets/plugins/sparklines/jquery.sparklines.min.js"></script> 			 <!-- Sparkline -->

        <script src="../theme-xtheme/assets/plugins/codeprettifier/prettify.js"></script> 				<!-- Code Prettifier  -->
        <script src="../theme-xtheme/assets/plugins/bootstrap-switch/bootstrap-switch.js"></script> 		<!-- Swith/Toggle Button -->

        <script src="../theme-xtheme/assets/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>  <!-- Bootstrap Tabdrop -->

        <script src="../theme-xtheme/assets/plugins/iCheck/icheck.min.js"></script>     					<!-- iCheck -->

        <script src="../theme-xtheme/assets/plugins/nanoScroller/js/jquery.nanoscroller.min.js"></script> <!-- nano scroller -->

        <script src="../theme-xtheme/assets/js/application.js"></script>
        <script src="../theme-xtheme/assets/demo/demo.js"></script>
        <script src="../theme-xtheme/assets/demo/demo-switcher.js"></script>

        <!-- End loading site level scripts -->

        <!-- Load page level scripts-->

        <script>
            $("#dyna").click(function () {
                if ($("#dynamo").text() == "")
                    $("#dynamo").text("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
                else {
                    $("#dynamo").html($("#dynamo").text() + $("#dynamo").text());
                }
            })

            $("#dyna-del").click(function () {
                $("#dynamo").html("");
            })	
        </script>
        
        <script>
    $(document).ready(function(){
        $(document).on('keyup','#search',function(){
            var $rows = $('#data_table .info');
            var orginal_row=0;
            var i=0;
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
            $rows.show().filter(function() {
                var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
                if(!~text.indexOf(val)==true){
                    i++;
                };
                orginal_row++;
                return !~text.indexOf(val);
            }).hide();
            //total_row=parseInt($rows)-i;
            $("#row_counter").html(orginal_row-i);
        });
        
        $(document).on("click",".btn-print_this_page",function(){
            content=$(this).attr("content");
            $(content).print();
        });
        $(".change_theme").click(function(){
            theme=$(this).attr("theme");
            page="../panel/theme/"+theme;
            
            window.localStorage.clear();
            page_refresh(page);
        })
    })
</script>

<style>
    .print_table {
        border-collapse: collapse;
        width:100%;
        border: 1px #ccc solid;
        margin-bottom: 20px;
    }
    .print_table td,.print_table th {
        border: 1px solid #ccc;
        height: 25px !important;
    }
    .print_table th{
        text-align: left !important;
    }
    .print_table tr{
        min-height: 50px !important;
    }
</style>

        <!-- End loading page level scripts-->


    </body>
</html>