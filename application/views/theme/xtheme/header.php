
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Banking Admin</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="description" content="december Admin Theme">
        <meta name="author" content="KaijuThemes">

        <link type='text/css' href='http://fonts.googleapis.com/css?family=Roboto:300,400,400italic,600,700' rel='stylesheet'>

        <link href="../theme-xtheme/assets/fonts/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">        <!-- Font Awesome -->
        <link href="../theme-xtheme/assets/fonts/ionicons/css/ionicons.min.css" type="text/css" rel="stylesheet">
        <link href="../theme-xtheme/assets/css/styles.css" type="text/css" rel="stylesheet">                                     <!-- Core CSS with all styles -->

        <link href="../theme-xtheme/assets/plugins/codeprettifier/prettify.css" type="text/css" rel="stylesheet">                <!-- Code Prettifier -->
        <link href="../theme-xtheme/assets/plugins/iCheck/skins/minimal/blue.css" type="text/css" rel="stylesheet">              <!-- iCheck -->

        <!--[if lt IE 10]>
            <script src="../xtheme/assets/js/media.match.min.js"></script>
            <script src="../xtheme/assets/js/respond.min.js"></script>
            <script src="../xtheme/assets/js/placeholder.min.js"></script>
        <![endif]-->

        <script src="../theme-xtheme/assets/js/jquery-1.10.2.min.js"></script> 							<!-- Load jQuery -->
        <script src="../theme-xtheme/assets/js/jqueryui-1.10.3.min.js"></script> 							<!-- Load jQueryUI -->
        <script src="../theme-xtheme/assets/js/bootstrap.min.js"></script> 
        <script src="../dist/js/customize.js"></script>
        <script src="../dist/js/print.js"></script>

    <body class="infobar-overlay sidebar-hideon-collpase sidebar-scroll animated-content">

        <header id="topnav" class="navbar navbar-teal navbar-fixed-top" role="banner">

            <div class="logo-area">		
                <a class="" href="#" style="color:white !important; margin-top: 15px;margin-left: 14px">
                    <!--<img class="img-white" alt="December" src="../xtheme/assets/img/logo-white-big.png">
                    <img class="img-dark" alt="December" src="../xtheme/assets/img/logo-big.png">-->

                    <?php
                    echo" WELCOME " . strtoupper($_SESSION['user_type']);
                    ?>


                </a>

                <span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
                    <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
                        <span class="icon-bg">
                            <i class="ion-navicon-round"></i>
                        </span>
                    </a>
                </span>


                <ul class="nav navbar-nav hidden-xs">
                    <!-- <li>
                            <a href="#"><i class="ion-plus-round"></i> New project</a>
                    </li> -->
                          <li class="toolbar-icon-bg hidden-xs" id="trigger-fullscreen">
                        <a href="#" class="toggle-fullscreen" ><span class="icon-bg"><i class="ion-android-expand"></i></span></i></a>
                    </li>
                    <li class="dropdown toolbar-icon-bg hidden-xs">
                    <a href="#" data-toggle="dropdown"><span class="icon-bg"><i class="ion-android-globe"></i></span></i></a>	
                     <li class="toolbar-icon-bg visible-xs-block" id="trigger-toolbar-search">
                    <a href="#"><span class="icon-bg"><i class="ion-search"></i></span></a>
                </li>
                <li>
                    <div class="toolbar-icon-bg hidden-xs" id="toolbar-search">
                    <div class="input-group">
                        <span class="input-group-btn"><button class="btn" type="button"><i class="ion-search"></i></button></span>
                        <input id="search" type="text" class="form-control" placeholder="Search...">
                        <span class="input-group-btn"><button class="btn" type="button"><i class="ion-close-round"></i></button></span>
                    </div>
                </div>
                </li>
                    <!--<ul class="dropdown-menu userinfo">
                        <li><a href="#/"><i class="ion-android-person"></i><span>Profile</span></a></li>
                        <li><a href="#/"><i class="ion-wrench"></i><span>Account</span></a></li>
                        <li><a href="#/"><i class="ion-gear-b"></i><span>Settings</span></a></li>
                        <li class="divider"></li>
                        <li><a href="#/"><i class="ion-stats-bars"></i><span>Earnings</span></a></li>
                        <li><a href="#/"><i class="ion-ios-list-outline"></i><span>Statement</span></a></li>
                        <li><a href="#/"><i class="ion-social-usd"></i><span>Withdrawals</span></a></li>
                        <li class="divider"></li>
                        <li><a href="#/"><i class="ion-power"></i><span>Sign Out</span></a></li>
                    </ul>-->
                </li>
                   

                </ul>

                
            </div><!-- logo-area -->

            <ul class="nav navbar-nav toolbar pull-right">

               
                
                
                <li class="dropdown toolbar-icon-bg hidden-xs">
                        <a class="dropdown-toggle nav-username" data-toggle="dropdown">
                              <span class="hidden-sm">Change Theme <i class="fa fa-angle-down"></i></span>
                                       
                        </a>
                        <ul class="dropdown-menu userinfo">
                           <li>
                                <a href="#" class="change_theme" theme="xtheme">
                                    <i class="fa fa-fw fa-weibo"></i><span data-localizem="theme_avenger">Xtheme</span></a>
                            </li>
          
                             <li>
                                <a href="#" class="change_theme" theme="gentelella">
                                    <i class="fa fa-fw fa-circle-o"></i><span data-localize="theme_teselella">Gentelella</span></a>
                            </li>
                        </ul>
                        <!-- /.dropdown-alerts -->
                    </li>
               
           
                
                
                
                 <li class="dropdown toolbar-icon-bg hidden-xs">
                    <a href="#" class="dropdown-toggle nav-username" data-toggle="dropdown">
                        <!--<img class="img-circle" src="../xtheme/assets/demo/avatar/avatar_16.png" alt="" />-->
                        <span class="hidden-sm">Database <i class="fa fa-angle-down"></i></span>
                    </a>			
                    <ul class="dropdown-menu userinfo">
                        <li>
                            <a href='http://localhost/phpmyadmin/#PMAURL-2:db_export.php?db=banking&table=&server=1&target=&token=4b812e06a5eec956e69985dea8203ea1' target="_blank"> <i class="fa fa-exchange"></i><span>Export</span></a>
                        </li>

                        <li>
                            <a href='http://localhost/phpmyadmin/#PMAURL-3:db_import.php?db=banking&table=&server=1&target=&token=4b812e06a5eec956e69985dea8203ea1' target="_blank"> <i class="fa fa-venus-mars"></i><span>Import</span></a>
                        </li>
                    </ul>
                </li>



                <li class="dropdown toolbar-icon-bg hidden-xs">
                    <a href="#" class="dropdown-toggle nav-username" data-toggle="dropdown">
                        <img class="img-circle" src="../theme-xtheme/assets/demo/avatar/avatar_16.png" alt="" />
                        <span class="hidden-sm"><?php echo $_SESSION['user_type'] ?> <i class="fa fa-caret-down"></i></span>
                    </a>			
                    <ul class="dropdown-menu userinfo">
                        <?php
                        if ($_SESSION['user_type'] == 'admin') {
                            echo"<li><a href='../panel/profile'><i class='ion-android-person'></i><span>Profile</span></a></li>";
                        }
                        ?>

                        <?php
                        if ($_SESSION['user_type'] == 'admin') {
                            echo"<li><a href='../panel/user_management'><i class='ion-android-person'></i><span>User Management</span></a></li>";
                        }
                        ?>
                        <li><a href="../login/signout"><i class="ion-power"></i><span>Sign Out</span></a></li>
                    </ul>
                </li>

                <li class="toolbar-icon-bg" id="trigger-infobar">
                    <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
                        <span class="icon-bg">
                            <i class="ion-more"></i>
                        </span>
                    </a>
                </li>
            </ul>

        </header>

        <div id="wrapper">
            <div id="layout-static">
                <div class="static-sidebar-wrapper sidebar-teal">
                    <div class="static-sidebar">
                        <div class="sidebar">
                            <!--<div class="widget stay-on-collapse" id="widget-profileinfo">
                                <div class="widget-body">
                                    <div class="userinfo">
                                        <div class="dropdown user-dropdown">
                                            <a class="dropdown-toggle"data-toggle="dropdown"><span class="button-icon"><i class="fa fa-cog"></i></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#/"><i class="fa fa-user"></i><span>View Profile</span></a></li>
                                                <li><a href="#/"><i class="fa fa-wrench"></i><span>Edit User</span></a></li>
                                                <li><a href="#/"><i class="fa fa-cog"></i><span>Settings</span></a></li>
                                                <li><a href="#/"><i class="fa fa-power-off"></i><span>Sign Out</span></a></li>
                                            </ul>
                                        </div>
                                        <div class="avatar">
                                            <img src="../xtheme/assets/demo/avatar/avatar_16.png" class="img-responsive img-circle"> 
                                        </div>
                                        <div class="info">
                                            <span class="username">Jonathan Smith</span>
                                            <span class="useremail">jon@december.com</span>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="widget stay-on-collapse" id="widget-sidebar">
                                <nav role="navigation" class="widget-body">
                                    <ul class="acc-menu">
                                        <li class="nav-separator"><span>Navigation</span></li>
                                        <li><a href="../panel/"><span class="icon"><i class="ion-android-home"></i></span><span>Dashboard</span></a></li>

                                        <li><a href="javascript:;"><span class="icon"><i class="ion-flash"></i></span><span>Member</span></a>
                                            <ul class="acc-menu">
                                                <li><a href="../member/current_member">Current Member</a></li>
                                                <li><a  href="../member/previous_member">Previous Member</a></li>
                                                <?php
                                                if ($_SESSION['user_type'] != "sales") {
                                                    echo"<li><a  href='../member/add_member'>Add Member</a></li>";
                                                }
                                                ?>

                                            </ul>
                                        </li>

                                        <li><a href='javascript:;'><span class='icon'><i class='ion-settings'></i></span><span>Open Account</span></a>
                                            <ul class="acc-menu">
                                                <li><a  href='../member/open_savings_account'>Open Savings Account</a></li>
                                                <li><a  href='../member/open_loan_account'>Open Loan Account</a></li>
                                            </ul>
                                        </li>
                                        <li><a href='javascript:;'><span class='icon'><i class="ion-android-pin"></i></span><span>Account Head/Subhead</span></a>
                                            <ul class="acc-menu">
                                                <li>
                                                    <a  href='../account/'>
                                                        <span>Account Head</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a  href='../account/account_subhead'>
                                                        <span>Account Sub Head</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-separator"><span>Accounts</span></li>
                                        <li><a href="javascript:;"><span class="icon"><i class="ion-shuffle"></i></span><span>Supplementary</span></a>
                                            <ul class="acc-menu">
                                                <li><a  href="../transaction/open_supplementary">Add Supplementary1</a></li>
                                                <li><a  href="../transaction/open_supplementary2">Add Supplementary2</a></li>
                                                <li><a  href="../account/view_supplementary1">View Supplementary1</a></li>
                                                <li><a  href="../account/view_supplementary2">View Supplementary2</a></li>
                                            </ul>
                                        </li>

                                        <li><a href="javascript:;"><span class="icon"><i class="ion-android-create"></i></span><span>Accounts</span></a>
                                            <ul class="acc-menu">
                                                <li><a  href="../member/all_savings_account">Savings Account</a></li>
                                                <li><a  href="../member/all_loan_account">Loan Account</a></li>
                                               <!-- <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i>FDR</a></li>-->
                                            </ul>
                                        </li>

                                        <li><a href="javascript:;"><span class="icon"><i class="ion-grid"></i></span><span>General Ledger</span></a>
                                            <ul class="acc-menu">
                                                <li><a  href="../transaction/get_general_ledger_data?account=S.B">S.B Account</a></li>
                                                <li><a  href="../transaction/get_general_ledger_data?account=Loan"> Loan Account</a></li>
                                                <li><a  href="../transaction/others_account">Other A/C Head</a></li>
                                                <li><a  href="../transaction/others_account_subhead">Other A/C Sub-Head</a></li>
                                                <li><a  href="../account/cash_in_hand_gl">Cash In Hand</a></li>
                                            </ul>
                                        </li>

                                        <li><a href="javascript:;"><span class="icon"><i class="ion-grid"></i></span><span>Summary</span></a>
                                            <ul class="acc-menu">
                                                <li><a  href="../transaction/get_ledger_summary?trans_type=dr">Debit</a></li>
                                                <li><a  href="../transaction/get_ledger_summary?trans_type=cr">Credit</a></li>
                                                <!--<li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>-->
                                            </ul>
                                        </li>


                                        <li>
                                            <a  href="../account/affair">
                                                <span class="icon"><i class="fa fa-envelope"></i></span> <span>Affair</span>
                                            </a>
                                        </li>
                                        <li><a  href="../account/set_loan_profit_return">
                                                <span class="icon"><i class="fa fa-bars"></i></span> <span>Set Interest</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a  href="../account/cash_position_memo">
                                                <span class="icon"><i class="fa fa-yen"></i></span> <span>Cash Position Memo</span>
                                            </a>
                                        </li>
                                         <li><a href="../login/signout"><span class="icon"><i class="ion-power"></i></span><span>Sign Out</span></a></li>


                                    </ul>
                                </nav>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="static-content-wrapper">
                    <div class="static-content">
                        <div class="page-content">

                            <div class="container-fluid">
                                <?php
                                if ($_SESSION['user_type'] == 'admin') {
                                    include_once 'bank_heading.php';
                                }
                                ?>
                                