-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2016 at 08:21 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `banking`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_head`
--

CREATE TABLE IF NOT EXISTS `account_head` (
`serial` int(11) NOT NULL,
  `acc_type` varchar(100) NOT NULL,
  `acc_head` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account_head`
--

INSERT INTO `account_head` (`serial`, `acc_type`, `acc_head`) VALUES
(1, 'asset', 'Income A/C'),
(27, 'liability', 'Expense');

-- --------------------------------------------------------

--
-- Table structure for table `acc_subhead`
--

CREATE TABLE IF NOT EXISTS `acc_subhead` (
`serial` int(11) NOT NULL,
  `acc_head` varchar(50) NOT NULL,
  `acc_subhead` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acc_subhead`
--

INSERT INTO `acc_subhead` (`serial`, `acc_head`, `acc_subhead`) VALUES
(1, 'Income A/C', 'interest'),
(2, 'Income A/C', 'service charge'),
(5, 'Expense', 'Rent'),
(6, 'Expense', 'Salary');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`serial` int(11) NOT NULL,
  `user_type` varchar(11) NOT NULL DEFAULT 'sales',
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `bank_name` varchar(50) NOT NULL DEFAULT 'no',
  `phone_no` varchar(20) NOT NULL DEFAULT 'no',
  `address` varchar(100) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`serial`, `user_type`, `name`, `password`, `bank_name`, `phone_no`, `address`) VALUES
(1, 'admin', 'admin', '123', 'Faith multi purpuse co-operative', '01737111299', 'Talaimari,kajla'),
(2, 'sales', 'sales', '123', 'Faith multi purpuse co-operative', '01737111299', 'Talaimari,kajla'),
(3, 'manager', 'manager', '123', 'Faith multi purpuse co-operative', '01737111299', 'Talaimari,kajla');

-- --------------------------------------------------------

--
-- Table structure for table `general_ledger`
--

CREATE TABLE IF NOT EXISTS `general_ledger` (
`serial` int(11) NOT NULL,
  `account_name` varchar(50) NOT NULL,
  `date` varchar(20) NOT NULL,
  `dr` double NOT NULL DEFAULT '0',
  `cr` double NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '1',
  `acc_type` varchar(15) NOT NULL,
  `time` varchar(30) NOT NULL,
  `particular` varchar(20) NOT NULL DEFAULT 'undefined',
  `acc_subhead` varchar(50) NOT NULL DEFAULT 'undefined'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `loan_account`
--

CREATE TABLE IF NOT EXISTS `loan_account` (
  `member_no` varchar(20) NOT NULL,
  `account_number` varchar(20) NOT NULL,
  `opening_date` varchar(20) NOT NULL,
  `comment` text NOT NULL,
  `status` varchar(15) NOT NULL DEFAULT 'current',
  `opening_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `loan_account_ledger`
--

CREATE TABLE IF NOT EXISTS `loan_account_ledger` (
`serial` int(11) NOT NULL,
  `member_no` varchar(15) NOT NULL,
  `account_no` varchar(20) NOT NULL,
  `dr` double NOT NULL DEFAULT '0',
  `cr` double NOT NULL DEFAULT '0',
  `date` varchar(20) NOT NULL,
  `comment` varchar(200) NOT NULL,
  `userid` int(11) NOT NULL DEFAULT '1',
  `cash` double NOT NULL DEFAULT '0',
  `transfer` double NOT NULL DEFAULT '0',
  `time` varchar(30) NOT NULL,
  `particular` varchar(15) NOT NULL DEFAULT 'By',
  `trans_type` varchar(20) NOT NULL DEFAULT 'nett'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `member_info`
--

CREATE TABLE IF NOT EXISTS `member_info` (
  `member_no` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `father_name` varchar(20) NOT NULL,
  `mother_name` varchar(20) NOT NULL,
  `current_address` text NOT NULL,
  `permanent_address` text NOT NULL,
  `birth_date` varchar(20) NOT NULL,
  `opening_date` varchar(20) NOT NULL,
  `age` int(11) NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `religion` varchar(20) NOT NULL,
  `occupation` varchar(30) NOT NULL,
  `education` text NOT NULL,
  `phone_no` varchar(15) NOT NULL,
  `status` varchar(12) NOT NULL DEFAULT 'current',
  `profile_img` varchar(20) NOT NULL DEFAULT 'default.jpg',
  `signature_img` varchar(20) NOT NULL DEFAULT 'default.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `others_account_ledger`
--

CREATE TABLE IF NOT EXISTS `others_account_ledger` (
`serial` int(11) NOT NULL,
  `dr` double NOT NULL DEFAULT '0',
  `cr` double NOT NULL DEFAULT '0',
  `date` varchar(16) NOT NULL,
  `comment` varchar(50) NOT NULL,
  `cash` double NOT NULL,
  `transfer` double NOT NULL,
  `time` varchar(20) NOT NULL,
  `particular` varchar(30) NOT NULL,
  `acc_subhead` varchar(100) NOT NULL,
  `acc_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `savings_account`
--

CREATE TABLE IF NOT EXISTS `savings_account` (
  `member_no` varchar(20) NOT NULL,
  `account_number` varchar(20) NOT NULL,
  `opening_amount` int(20) NOT NULL,
  `opening_date` varchar(20) NOT NULL,
  `comment` text NOT NULL,
  `status` varchar(15) NOT NULL DEFAULT 'current'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `savings_account_ledger`
--

CREATE TABLE IF NOT EXISTS `savings_account_ledger` (
`serial` int(11) NOT NULL,
  `member_no` varchar(20) NOT NULL,
  `account_no` varchar(20) NOT NULL,
  `dr` double NOT NULL DEFAULT '0',
  `cr` double NOT NULL DEFAULT '0',
  `date` varchar(20) NOT NULL,
  `comment` varchar(200) NOT NULL DEFAULT 'No Comment',
  `userid` int(11) NOT NULL DEFAULT '1',
  `cash` double NOT NULL DEFAULT '0',
  `transfer` double NOT NULL DEFAULT '0',
  `time` varchar(30) NOT NULL,
  `particular` varchar(15) NOT NULL DEFAULT 'By',
  `trans_type` varchar(11) NOT NULL DEFAULT 'nett'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `set_loan_return`
--

CREATE TABLE IF NOT EXISTS `set_loan_return` (
`serial` int(11) NOT NULL,
  `loan_account_no` varchar(30) NOT NULL,
  `start_date` varchar(30) NOT NULL,
  `end_date` varchar(30) NOT NULL,
  `amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_head`
--
ALTER TABLE `account_head`
 ADD PRIMARY KEY (`serial`), ADD UNIQUE KEY `acc_head` (`acc_head`);

--
-- Indexes for table `acc_subhead`
--
ALTER TABLE `acc_subhead`
 ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `general_ledger`
--
ALTER TABLE `general_ledger`
 ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `loan_account`
--
ALTER TABLE `loan_account`
 ADD PRIMARY KEY (`account_number`);

--
-- Indexes for table `loan_account_ledger`
--
ALTER TABLE `loan_account_ledger`
 ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `member_info`
--
ALTER TABLE `member_info`
 ADD PRIMARY KEY (`member_no`);

--
-- Indexes for table `others_account_ledger`
--
ALTER TABLE `others_account_ledger`
 ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `savings_account`
--
ALTER TABLE `savings_account`
 ADD PRIMARY KEY (`account_number`);

--
-- Indexes for table `savings_account_ledger`
--
ALTER TABLE `savings_account_ledger`
 ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `set_loan_return`
--
ALTER TABLE `set_loan_return`
 ADD PRIMARY KEY (`serial`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_head`
--
ALTER TABLE `account_head`
MODIFY `serial` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `acc_subhead`
--
ALTER TABLE `acc_subhead`
MODIFY `serial` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `serial` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `general_ledger`
--
ALTER TABLE `general_ledger`
MODIFY `serial` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `loan_account_ledger`
--
ALTER TABLE `loan_account_ledger`
MODIFY `serial` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `others_account_ledger`
--
ALTER TABLE `others_account_ledger`
MODIFY `serial` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `savings_account_ledger`
--
ALTER TABLE `savings_account_ledger`
MODIFY `serial` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `set_loan_return`
--
ALTER TABLE `set_loan_return`
MODIFY `serial` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
