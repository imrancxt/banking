
$(document).ready(function(){
    $(document).on('submit','#login_form',function(e)
    {
        var formObj =$(this);
        var formURL =formObj.attr("action");
        var formData = new FormData(this);
        $.ajax({
            url: formURL,
            type: 'POST',
            data:formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            success: function(data, textStatus, jqXHR)
            {
                  if(data=="success"){
                      url="http://localhost/banking/panel/";
                      window.location=url;
                  }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
            }
                        
        });
        e.preventDefault();
    });
});